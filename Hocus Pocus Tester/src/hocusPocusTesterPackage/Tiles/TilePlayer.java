package hocusPocusTesterPackage.Tiles;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

import java.awt.Rectangle;

//This is an tile that represents the player's position on the game.  It is used in
//conjunction with the Player class to form the info for the player.  This is used to
//fo tile movement and other tile calculations.
public class TilePlayer extends BaseTile
{
	public TilePlayer()
	{
		walkable = true;
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TilePlayer(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TilePlayer(TilePlayer tilePlayer)
	{
		walkable = true;
		tileBoundingBox = new Rectangle((int)(tilePlayer.getBoundingBox().getX()), (int)(tilePlayer.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update() 
	{
	}

	public void onEnter(Level level) 
	{
	}

	public void onExit(Level level) 
	{	
	}

	public void onTriggerEnter(Level level) 
	{
	}

	@Override
	public BaseTile deepCopy() 
	{
		return new TilePlayer(this);
	}
}
