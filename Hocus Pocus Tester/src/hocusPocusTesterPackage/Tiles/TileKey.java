package hocusPocusTesterPackage.Tiles;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

import hocusPocusTesterPackage.GlobalConstants;
import hocusPocusTesterPackage.Level;

//The key tile unlocks the win tile so that the next level can be played.
//The Trigger Handler will take care of its job so all this tile has 
//to do is look pretty.
public class TileKey extends BaseTile
{
	private final static String TILE_KEY_SPRITE_FILENAME = "Art/Tile Key.png";
	private final static String TILE_WALKABLE_SPRITE_FILENAME = "Art/Tile Walkable.png";

	private Image floorSprite;
	
	public TileKey()
	{
		walkable = true;
		tileBoundingBox = new Rectangle(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_KEY_SPRITE_FILENAME).getImage();
		floorSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public TileKey(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rectangle(xPos, yPos, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_KEY_SPRITE_FILENAME).getImage();
		floorSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public TileKey(TileKey oldTileKey)
	{
		walkable = true;
		tileBoundingBox = new Rectangle((int)(oldTileKey.getBoundingBox().getX()), (int)(oldTileKey.getBoundingBox().getY()), GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = new ImageIcon(TILE_KEY_SPRITE_FILENAME).getImage();
		floorSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
		triggered = false;
	}
	
	public void update() 
	{
	}

	public void onEnter(Level level) 
	{
		tileSprite = new ImageIcon(TILE_WALKABLE_SPRITE_FILENAME).getImage();
	}

	public void onExit(Level level) 
	{
	}

	public void onTriggerEnter(Level level) 
	{
	}

	@Override
	public BaseTile deepCopy() 
	{
		return new TileKey(this);
	}
	
	public void paint(Graphics mainGraphic) 
	{
		Graphics2D tileGraphic = (Graphics2D)mainGraphic;
		tileGraphic.drawImage(floorSprite, tileBoundingBox.x, tileBoundingBox.y, null);
		tileGraphic.drawImage(tileSprite, tileBoundingBox.x, tileBoundingBox.y, null);
	}
}
