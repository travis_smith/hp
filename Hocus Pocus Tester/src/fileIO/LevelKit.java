package fileIO;

import java.util.ArrayList;



public class LevelKit 
{
	public final static int CURRENT_VERSION = 3;
	
	private LevelData levelData;
	private TileData tileData[][][];
	private ArrayList<TriggerData> triggers;
	
	public LevelKit(LevelData lData, TileData tData[][][], ArrayList<TriggerData> triggers)
	{
		this.levelData = lData;
		this.tileData = tData;
		this.triggers = triggers;
	}
	
	public LevelData getLevelData()
	{
		return this.levelData;
	}
	
	public TileData[][][] getTileData()
	{
		return this.tileData;
	}
	
	public ArrayList<TriggerData> getTriggers()
	{
		return triggers;
	}
}
