package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

//This is a Tile Win.  It ends the game.  In case of the tester it restarts the level.
public class TileWin extends BaseTile
{
	//The Win Tile art filename.
    private final static String TILE_WIN_CLOSED_SPRITE_FILENAME = "res/drawable/tilewinclosed.png";
    private final static String TILE_WIN_OPEN_SPRITE_FILENAME = "res/drawable/tilewinopen.png";
    private final static String TILE_WALKABLE_SPRITE_FILENAME = "res/drawable/tilewalkable.png";
    
    private boolean isOpen;
    
    InputStream winClosedStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WIN_CLOSED_SPRITE_FILENAME);
    InputStream winOpenStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WIN_OPEN_SPRITE_FILENAME);
    InputStream walkStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALKABLE_SPRITE_FILENAME);
    
    private Bitmap floorSprite;
    
	public TileWin()
	{
		walkable = true;
		
		tileSprite = BitmapFactory.decodeStream(winClosedStream);
		floorSprite = BitmapFactory.decodeStream(walkStream);
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
		isOpen = false;
	}
	
	public TileWin(int xPos, int yPos, boolean open)
	{
		walkable = false;
		if(!open)
		{
			tileSprite = BitmapFactory.decodeStream(winClosedStream);
		}
		else
		{
			tileSprite = BitmapFactory.decodeStream(winOpenStream);
		}
		floorSprite = BitmapFactory.decodeStream(walkStream);
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos +GlobalConstants.getTileSize());
		triggered = false;
		isOpen = open;
	}
	
	public TileWin(TileWin oldTileWin)
	{
		this.walkable = oldTileWin.isWalkable();
		if(!oldTileWin.isOpen)
		{
			
			tileSprite = BitmapFactory.decodeStream(winClosedStream);
		}
		else
		{
			tileSprite = BitmapFactory.decodeStream(winOpenStream);
		}
		floorSprite = BitmapFactory.decodeStream(walkStream);
		this.tileBoundingBox = new Rect((int)(oldTileWin.getBoundingBox().left), (int)(oldTileWin.getBoundingBox().top), (int)(oldTileWin.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileWin.getBoundingBox().top) + GlobalConstants.getTileSize());
		this.triggered = oldTileWin.triggered;
		this.isOpen = oldTileWin.isOpen;
	}
	
	public void update()
	{
	}

	public void onEnter(Level level) 
	{
		if(isOpen)
		{
			level.levelDone();
			//level.loadNextLevel();
		}
	}

	public void onExit(Level level)
	{
	}

	public void onTriggerEnter(Level level) 
	{
		isOpen = true;
		//Clean the bitmap.
		if(tileSprite != null)
		{
			tileSprite.recycle();
		}
		tileSprite = BitmapFactory.decodeStream(winOpenStream);
	}
	
	//Override the draw here.
	public void draw(Canvas canvas) 
	{
		canvas.drawBitmap(floorSprite, tileBoundingBox.left, tileBoundingBox.top, null);
		
		if(tileSprite != null)
		{
			if(!isOpen)
			{
				canvas.drawBitmap(tileSprite, tileBoundingBox.left, tileBoundingBox.top, null);
			}
			else
			{
				canvas.drawBitmap(tileSprite, tileBoundingBox.left, tileBoundingBox.top, null);
			}
		}
	}
	
	public BaseTile deepCopy()
	{
		return new TileWin(this);
	}
	
	public void unlock()
	{
		isOpen = true;
		if(tileSprite != null)
		{
			tileSprite.recycle();
		}
		tileSprite = BitmapFactory.decodeStream(winOpenStream);
	}
}
