package hocusPocusTesterPackage.Tiles;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;

//The BaseTile is the tile with no functionality but
//shapes the tiles that extend it.
public abstract class BaseTile
{	
	protected boolean walkable;
	protected Rect tileBoundingBox;
	protected Bitmap tileSprite;
	protected boolean triggered;
	
	//The update logic loop for the tile.
	abstract public void update();
	//Executes when the Player enters the tile.
	abstract public void onEnter(Level level);
	//Executes when the Player leaves the tile.
	abstract public void onExit(Level level);
	//Executes when the Player activates this tile's trigger.
	abstract public void onTriggerEnter(Level level);
	//Return a deep copy of the tile.
	abstract public BaseTile deepCopy();
	
	public void moveTo(int xPos, int yPos) 
	{
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
	}
	
	public void draw(Canvas canvas) 
	{
		if(tileSprite != null)
		{
			canvas.drawBitmap(tileSprite, tileBoundingBox.left, tileBoundingBox.top, null); 
		}
	}
	
	public Rect getBoundingBox()
	{
		return tileBoundingBox;
	}
	
	public boolean isWalkable() 
	{
		return walkable;
	}
	
	//Compares tiles.  Tiles will never be placed on top of each other so
	//all this will do is compare the locations of the tiles.  Tiles that
	//are closer to the bottom right are bigger.  It goes tileOne - tileTwo.
	//Right now it is overloaded to allow the equals method to work; it does
	//not work for comparisons yet.
	public int compare(BaseTile tiletwo)
	{
		if(this.getBoundingBox().left == tiletwo.getBoundingBox().left && this.getBoundingBox().top == tiletwo.getBoundingBox().top)
		{
			return 0;
		}
		return -1;
	}
}
