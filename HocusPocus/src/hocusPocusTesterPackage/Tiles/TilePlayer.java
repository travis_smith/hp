package hocusPocusTesterPackage.Tiles;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.Rect;

//This is an tile that represents the player's position on the game.  It is used in
//conjunction with the Player class to form the info for the player.  This is used to
//for tile movement and other tile calculations.
public class TilePlayer extends BaseTile
{
	public TilePlayer()
	{
		walkable = true;
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TilePlayer(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TilePlayer(TilePlayer tilePlayer)
	{
		walkable = true;
		tileBoundingBox = new Rect((int)(tilePlayer.getBoundingBox().left), (int)(tilePlayer.getBoundingBox().top), (int)(tilePlayer.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(tilePlayer.getBoundingBox().top) + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public void update() 
	{
	}

	public void onEnter(Level level) 
	{
	}

	public void onExit(Level level) 
	{	
	}

	public void onTriggerEnter(Level level) 
	{
	}

	@Override
	public BaseTile deepCopy() 
	{
		return new TilePlayer(this);
	}
}
