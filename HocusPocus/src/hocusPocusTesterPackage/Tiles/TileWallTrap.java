package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.BitmapFactory;
import android.graphics.Rect;


//Tile Wall Traps are tiles that can only be walked over once.  Once they are
//passed they become walls tiles and can not be walked over again.
public class TileWallTrap extends BaseTile
{
	//The Wall Trap Tile art filename.
    private final static String TILE_WALL_TRAP_SPRITE_FILENAME = "res/drawable/tilewalltrap.png";
    //The Wall Tile art filename.
    private final static String TILE_WALL_SPRITE_FILENAME = "res/drawable/tilewall.png";
    
    InputStream wallTrapStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALL_TRAP_SPRITE_FILENAME);
    InputStream wallStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALL_SPRITE_FILENAME);
    
    
	public TileWallTrap()
	{
		walkable = true;
		tileSprite = BitmapFactory.decodeStream(wallTrapStream);
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWallTrap(int xPos, int yPos)
	{
		walkable = true;
		tileSprite = BitmapFactory.decodeStream(wallTrapStream);
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
		triggered = false;
	}
	
	public TileWallTrap(TileWallTrap oldTileWallTrap)
	{
		walkable = oldTileWallTrap.isWalkable();
		tileSprite = BitmapFactory.decodeStream(wallTrapStream);
		tileBoundingBox = new Rect((int)(oldTileWallTrap.getBoundingBox().left), (int)(oldTileWallTrap.getBoundingBox().top),  (int)(oldTileWallTrap.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileWallTrap.getBoundingBox().top) + GlobalConstants.getTileSize());
		triggered = false;
	}

	public void update() 
	{
	}

	public void onEnter(Level level)
	{
	}

	public void onExit(Level level)
	{
		triggered = false;
		walkable = false;
		tileSprite = BitmapFactory.decodeStream(wallStream);
	}

	public void onTriggerEnter(Level level) 
	{
		triggered = true;
		onExit(level);
	}

	public BaseTile deepCopy() 
	{
		return new TileWallTrap(this);
	}
}
