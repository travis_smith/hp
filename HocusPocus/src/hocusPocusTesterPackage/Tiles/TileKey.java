package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

//The key tile unlocks the win tile so that the next level can be played.
//The Trigger Handler will take care of its job so all this tile has 
//to do is look pretty.
public class TileKey extends BaseTile
{
	private final static String TILE_KEY_SPRITE_FILENAME = "res/drawable/tilekey.png";
	private final static String TILE_WALKABLE_SPRITE_FILENAME = "res/drawable/tilewalkable.png";
	
	InputStream keyStream = this.getClass().getClassLoader().getResourceAsStream(TILE_KEY_SPRITE_FILENAME);
	InputStream walkStream = this.getClass().getClassLoader().getResourceAsStream(TILE_WALKABLE_SPRITE_FILENAME);

	private Bitmap floorSprite;
	
	boolean taken;
	
	public TileKey()
	{
		walkable = true;
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(keyStream);
		floorSprite = BitmapFactory.decodeStream(walkStream);
		triggered = false;
		taken = false;
	}
	
	public TileKey(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(keyStream);
		floorSprite = BitmapFactory.decodeStream(walkStream);
		triggered = false;
		taken = false;
	}
	
	public TileKey(TileKey oldTileKey)
	{
		walkable = true;
		tileBoundingBox = new Rect((int)(oldTileKey.getBoundingBox().left), (int)(oldTileKey.getBoundingBox().top), (int)(oldTileKey.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileKey.getBoundingBox().top) + GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(keyStream);
		floorSprite = BitmapFactory.decodeStream(walkStream);
		triggered = false;
		taken = oldTileKey.taken;
	}
	
	public void update() 
	{
	}

	public void onEnter(Level level) 
	{
		taken = true;
	}

	public void onExit(Level level) 
	{
	}

	public void onTriggerEnter(Level level) 
	{
	}

	public BaseTile deepCopy() 
	{
		return new TileKey(this);
	}
	
	public void draw(Canvas canvas) 
	{
		canvas.drawBitmap(floorSprite, tileBoundingBox.left, tileBoundingBox.top, null);
		if(!taken)
		{
			canvas.drawBitmap(tileSprite, tileBoundingBox.left, tileBoundingBox.top, null);
		}
	}
}
