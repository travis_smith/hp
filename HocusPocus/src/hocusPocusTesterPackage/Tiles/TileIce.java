package hocusPocusTesterPackage.Tiles;

import java.io.InputStream;

import org.apache.http.conn.scheme.PlainSocketFactory;

import com.example.hocuspocus.GlobalConstants;
import com.example.hocuspocus.Level;

import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

//The ice tile forces the player to exit it in the same direction it was entered.
public class TileIce extends BaseTile
{
	private final static String TILE_ICE_SPRITE_FILENAME = "res/drawable/tileice.png";
	InputStream iceStream = this.getClass().getClassLoader().getResourceAsStream(TILE_ICE_SPRITE_FILENAME);
	
	public TileIce()
	{
		walkable = true;
		tileBoundingBox = new Rect(0, 0, GlobalConstants.getTileSize(), GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(iceStream);
		triggered = false;
	}
	
	public TileIce(int xPos, int yPos)
	{
		walkable = true;
		tileBoundingBox = new Rect(xPos, yPos, xPos + GlobalConstants.getTileSize(), yPos + GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(iceStream);
		triggered = false;
	}
	
	public TileIce(TileIce oldTileIce)
	{
		walkable = true;
		tileBoundingBox = new Rect((int)(oldTileIce.getBoundingBox().left), (int)(oldTileIce.getBoundingBox().top), (int)(oldTileIce.getBoundingBox().left) + GlobalConstants.getTileSize(), (int)(oldTileIce.getBoundingBox().top) + GlobalConstants.getTileSize());
		tileSprite = BitmapFactory.decodeStream(iceStream);
		triggered = false;
	}
	
	public void update() 
	{
	}

	//Move the player one more if possible in the same direction they entered.
	public void onEnter(Level level) 
	{
		int xDifference = (int)(level.getPlayer().getBoundingBox().left - level.getPreviousPlayerTile().getBoundingBox().left);
		int yDifference = (int)(level.getPlayer().getBoundingBox().top - level.getPreviousPlayerTile().getBoundingBox().top);
		
		Log.d("Han", "Diff X: " + xDifference + " Y: " + yDifference);
		
		Rect movementRect = new Rect((int)(level.getPlayer().getBoundingBox().left + xDifference), (int)(level.getPlayer().getBoundingBox().top + yDifference), (int)(level.getPlayer().getBoundingBox().left + xDifference) + 1, (int)(level.getPlayer().getBoundingBox().top + yDifference) + 1);
		
		if(level.getTileAt(movementRect).getBoundingBox().left != GlobalConstants.getNotANumber() && level.getTileAt(movementRect).isWalkable())
		{
			Log.d("Han", "Tile X: " + level.getTileAt(movementRect).getBoundingBox().left + " Y: " + level.getTileAt(movementRect).getBoundingBox().top);
			level.movePlayer((int)(level.getPlayer().getBoundingBox().left + xDifference), (int)(level.getPlayer().getBoundingBox().top + yDifference));
			//Path is wrong so invalidate it.
			level.clearPath();
		}
	}

	public void onExit(Level level) 
	{		
	}

	public void onTriggerEnter(Level level) 
	{		
	}

	public BaseTile deepCopy()
	{
		return new TileIce(this);
	}
	
}
