package com.example.hocuspocus;

import hocusPocusTesterPackage.Tiles.BaseTile;
import hocusPocusTesterPackage.Tiles.TileIce;
import hocusPocusTesterPackage.Tiles.TileKey;
import hocusPocusTesterPackage.Tiles.TilePlayer;
import hocusPocusTesterPackage.Tiles.TileWall;
import hocusPocusTesterPackage.Tiles.TileWallTrap;
import hocusPocusTesterPackage.Tiles.TileWin;

import java.io.InputStream;
import java.util.ArrayList;

import com.example.hocuspocus.GlobalConstants.SwipeDirection;

import android.content.Context;
import android.gesture.Gesture;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnTouchListener;

//The Level class is responsible for the game logic and graphics.
//It runs the games logic update loop and houses the JPanel that the
//games graphics are drawn on to.

public class Level extends View implements OnTouchListener
{	
	//A pointer to the engine.  Needed to load other levels.
	private Engine engine;
	
	//The current Player that is navigating through the game.
	private Player player;
	//Where the player needs to be.
	private Point playerDestination;
	//The spawn of the player.
	private Point playerSpawn;
	//The path for the player.
	private ArrayList<BaseTile> path;
	//The previous tile that the Player was on.  Only changes on movement.
	private TilePlayer previousPlayerTile;
	
	//The point at which the mouse was clicked.
	private Rect mousePosition;
	//The point that we will use to see where the swipe direction is in.
	private Rect mouseRotLocation;
	//The boolean to see if we set the mouse position to initalize the
	//rotation.
	private boolean mouseRotPosInit;
	
	//How many panels total across.
	private int width;
	//How many panels total down.
	private int height;
	//The list of Level Panels.  Use the width to figure out the layout.
	private ArrayList<LevelPanel> listOfLevelPanels;
	//Save the level panels for resetting.
	private ArrayList<LevelPanel> savedListOfPanels;
	//Save the location of the triggers.  The first rect in each subarray is
	//the trigger and the remaining is the list of tiles that get triggered.
	private ArrayList<ArrayList<Rect>> savedTriggers;
	//The list of rings in order from outer to inner.
	private ArrayList<ArrayList<LevelPanel>> listOfRings;
	//The trigger system for the level.
	private TriggerHandler triggerHandler;
	//Is the level done already?
	private boolean levelDone;
	
	//Is there a level to load.
	private boolean loading;
	
	//Has updating finished.
	private boolean updatingFinished;
	//Has painting finished.
	private boolean paintingFinished;
	
	//Has the mouse been pressed.
	private boolean mouseHeld;
	
	//Has the mouse been clicked on the player and the game moved to the Level Panel
	//rotating stage.
	private boolean levelRotation;
	private boolean doneRotating;
	
	//The time.  Used to check for the update time rate.
	private int timeElapsed;
	//The last time since the last time check.
	private int lastTimeUpdate;
	//The amount of time(milliseconds) to wait before moving to the next tile in the path.
	private static final int TIME_BETWEEN_TILE_MOVEMENT = 250;
	//The file name for the rotation vortex sprite.
	InputStream spiralVortexStream = this.getClass().getClassLoader().getResourceAsStream("res/drawable/spiralvortex.png");
	private final Bitmap ROTATION_VORTEX_SPRITE = BitmapFactory.decodeStream(spiralVortexStream);
	//The paint for the background.
	Paint paint;
	//The rect that is the size of the screen.
	Rect screenRect;
	//The threshold for a rotation to take place.
	private final int ROTATION_THRESHOLD = 50;

	//A handle to the engine running this level.
	private Engine currentEngine;
	
	//The dimensions are in level panels.
	public Level(Context context, Engine eng, int levelWidth, int levelHeight, int tilesInLevelPanel)
	{
		super(context);
		
		engine = eng;
		
		setOnTouchListener(this);
		setWillNotDraw(false);
		setFocusable(true);
		
		timeElapsed = 0;
		lastTimeUpdate = (int)System.currentTimeMillis();
		
		loading = false;
		mouseHeld = false;
		updatingFinished = true;
		paintingFinished = true;
		levelRotation = false;

		width = levelWidth;
		height = levelHeight;
		listOfLevelPanels = new ArrayList<LevelPanel>();
		savedListOfPanels = new ArrayList<LevelPanel>();
		savedTriggers = new ArrayList<ArrayList<Rect>>();
		listOfRings = new ArrayList<ArrayList<LevelPanel>>();
		levelDone = false;
				
		triggerHandler = new TriggerHandler();
		
		path = new ArrayList<BaseTile>();
		
		mousePosition = new Rect();
		mouseRotLocation = new Rect();
		mouseRotPosInit = false;
		
		player = new Player();
		playerSpawn = new Point(0, 0);
		playerDestination = new Point(0, 0);
		previousPlayerTile = new TilePlayer((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top));
		
		paint = new Paint();
		paint.setColor(Color.BLACK);
		
		screenRect = new Rect(0, 0, GlobalConstants.getLevelWidth(), GlobalConstants.getLevelHeight());
	}
	
	public void reloadLevel()
	{
		//Reset the level.
		path.clear();	
		
		listOfLevelPanels.clear();
		listOfRings.clear();
		
		triggerHandler.reset();

		mousePosition = new Rect();
		mouseRotLocation = new Rect();
		mouseRotPosInit = false;
		
		levelRotation = false;
		
		//Create the objects that will handle the Player information.
		player = new Player();
		playerDestination = new Point(playerSpawn.x, playerSpawn.y);
		player.moveTo((int)(playerSpawn.x), (int)(playerSpawn.y));
		previousPlayerTile = new TilePlayer((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top));
		
		//Copy the saved level to the used level.
		for(int savedPanelNumber = 0 ; savedPanelNumber < savedListOfPanels.size(); savedPanelNumber++)
		{
			LevelPanel levelPanel = savedListOfPanels.get(savedPanelNumber);
			listOfLevelPanels.add(levelPanel.deepCopy());
		}
		
		//Add the triggers back.
		for(int listOfTriggerNumber = 0; listOfTriggerNumber < savedTriggers.size(); listOfTriggerNumber++)
		{
			BaseTile triggerTile = getTileAt(savedTriggers.get(listOfTriggerNumber).get(0));
			
			ArrayList<BaseTile> triggeredTiles = new ArrayList<BaseTile>();
			
			for(int triggeredTileNumber = 0; triggeredTileNumber < savedTriggers.get(listOfTriggerNumber).size(); triggeredTileNumber++)
			{
				triggeredTiles.add(getTileAt(savedTriggers.get(listOfTriggerNumber).get(triggeredTileNumber)));
			}
			
			triggerHandler.addNewTrigger(triggerTile, triggeredTiles);
		}
		
		//
		TileWin doorTile = null;
		TileKey keyTile = null;
		for(int levelPanelNumber = 0; levelPanelNumber < this.getListOfLevelPanels().size(); levelPanelNumber++)
		{
			for(int panelNumber = 0; panelNumber < this.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().size(); panelNumber++)
			{
				if(this.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber) instanceof TileKey)
				{
					keyTile = (TileKey)this.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber);
				}
				else if(this.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber) instanceof TileWin)
				{
					doorTile = (TileWin)this.getListOfLevelPanels().get(levelPanelNumber).getListOfTiles().get(panelNumber);
				}
			}
		}
		
		if(keyTile == null)
		{
			doorTile.unlock();
		}
		
		//Find and fill the rings.
		//Fill in the array with place holders.
		createRings();
		
		//Add the planes to the level.  Must add in left to right and up and down order.
		loading = false;
		updatingFinished = false;
		paintingFinished = false;
		
		invalidate();
	}
	
	public void clearLevel()
	{
		path.clear();
		
		listOfLevelPanels.clear();
		savedListOfPanels.clear();
		savedTriggers.clear();
		listOfRings.clear();
		triggerHandler.reset();
		levelDone = false;
		
		loading = false;;
		
		updatingFinished = false;
		paintingFinished = false;;
	}
	
	private void createDummyLevel()
	{
		if(updatingFinished && paintingFinished)
		{
			//Reset the level.
			path.clear();	
			
			listOfLevelPanels.clear();
			listOfRings.clear();
			
			triggerHandler.reset();

			mouseHeld = false;
			
			levelRotation = false;
			
			//Create the objects that will handle the Player information.
			player = new Player();
			player.moveTo(0,0);
			playerSpawn = new Point(0,0);
			playerDestination = new Point(playerSpawn.x, playerSpawn.y);
			previousPlayerTile = new TilePlayer((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top));
			
			for(int ySection = 0; ySection < height*3; ySection+=3)
			{
				for(int xSection = 0; xSection < width*3; xSection+=3)
				{
					LevelPanel levelPanel = new LevelPanel(GlobalConstants.getTileSize()*xSection, GlobalConstants.getTileSize()*ySection, 3, 3);
					levelPanel.addTile(1 + (int)(Math.random() * ((2 - 1) + 1)), 1 + (int)(Math.random() * ((2 - 1) + 1)), new TileWall());
					
					if(xSection/3 == 0 && ySection/3 == 0)
					{
						levelPanel.addTile(1 + (int)(Math.random() * ((2 - 1) + 1)), 1 + (int)(Math.random() * ((2 - 1) + 1)), new TileWallTrap());
					}
					else if(xSection/3 == 1 && ySection/3 == 0)
					{
						levelPanel.addTile(1 + (int)(Math.random() * ((2 - 1) + 1)), 1 + (int)(Math.random() * ((2 - 1) + 1)), new TileIce());
					}
					else if(xSection/3 == 0 && ySection/3 == 1)
					{
						levelPanel.addTile(1, 1, new TileKey());
					}
					else if(xSection/3 == 1 && ySection/3 == 1)
					{
						levelPanel.addTile(2, 2, new TileWin());
					}
					addLevelPanel(levelPanel);
				}
			}
			
			ArrayList<BaseTile> listOfTriggeredTiles = new ArrayList<BaseTile>();
			listOfTriggeredTiles.add(listOfLevelPanels.get(3).getListOfTiles().get(8));
			triggerHandler.addNewTrigger(listOfLevelPanels.get(2).getListOfTiles().get(4), listOfTriggeredTiles);
			
			ArrayList<Rect> saveTrig = new ArrayList<Rect>();
			saveTrig.add(new Rect(listOfLevelPanels.get(2).getListOfTiles().get(4).getBoundingBox().left, listOfLevelPanels.get(2).getListOfTiles().get(4).getBoundingBox().top, listOfLevelPanels.get(2).getListOfTiles().get(4).getBoundingBox().right, listOfLevelPanels.get(2).getListOfTiles().get(4).getBoundingBox().bottom));
			saveTrig.add(new Rect(listOfTriggeredTiles.get(0).getBoundingBox().left, listOfTriggeredTiles.get(0).getBoundingBox().top, listOfTriggeredTiles.get(0).getBoundingBox().right, listOfTriggeredTiles.get(0).getBoundingBox().bottom));
			savedTriggers.add(saveTrig);
			
			//Add the planes to the level.  Must add in left to right and up and down order.
			updatingFinished = false;
			paintingFinished = false;
			loading = false;
		}
	}
	
	public void resetLevel()
	{
		loading = true;
		mouseHeld = false;
		reloadLevel();
	}
	
	//Adds/Replaces a Level Plane to the right place.
	public void addLevelPanel(LevelPanel newLevelPanel)
	{
		listOfLevelPanels.add(newLevelPanel);
		savedListOfPanels.add(newLevelPanel.deepCopy());
	}
	
	//Update the game's objects and run the game's logic.
	public void update()
	{
		updatingFinished = false;
		if(!loading)
		{
			doneRotating = true;
			
			for(int panelNumber = 0;panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				if(!listOfLevelPanels.get(panelNumber).isDoneMoving())
				{
					doneRotating = false;
				}
			}

			timeElapsed += System.currentTimeMillis() - lastTimeUpdate;
			lastTimeUpdate = (int) System.currentTimeMillis();
			
			if(mouseHeld)
			{
				if(doneRotating && !levelRotation)
				{
					if(path.isEmpty() && (player.getBoundingBox().left != mousePosition.left || player.getBoundingBox().top != mousePosition.top))
					{
						//Note:This is assuming that all level panels are the same sizes.
						PathFinding newPathFinder = new PathFinding(GlobalConstants.getLevelWidth()/GlobalConstants.getTileSize(), GlobalConstants.getLevelHeight()/GlobalConstants.getTileSize(), listOfLevelPanels);
						path = newPathFinder.findPath(new TilePlayer((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top)), new TilePlayer((int)(mousePosition.left), (int)(mousePosition.top)));
					}
		
					if(!path.isEmpty() && timeElapsed >= TIME_BETWEEN_TILE_MOVEMENT && !loading)
					{
						//Note:Calculation here depends on panels having the same sizes.  Using
						//the first panel in the array list.
						movePlayer((int)(path.get(path.size() - 1).getBoundingBox().left), (int)(path.get(path.size() - 1).getBoundingBox().top));
	
						if(!path.isEmpty())
						{
							path.remove(path.size() - 1);
							timeElapsed = 0;
						}
					}
				}
				else if(doneRotating)
				{
					//Get the bounding box to use for the dimension.
					Rect levelPanelBoundingBox = new Rect(listOfLevelPanels.get(0).getBoundingBoxArea().left, listOfLevelPanels.get(0).getBoundingBoxArea().top, listOfLevelPanels.get(0).getBoundingBoxArea().right, listOfLevelPanels.get(0).getBoundingBoxArea().bottom);
					
					//Create four rectangles that would cut the level into four areas.  The mouse movement will be
					//interpreted by which area the mouse is in.
					Rect topEdge = new Rect(0, 0, (int)(width*levelPanelBoundingBox.width()), (int)(levelPanelBoundingBox.height()*Math.floor(width/2)));
					Rect bottomEdge = new Rect(0, (int)((height * levelPanelBoundingBox.height()) - (levelPanelBoundingBox.height()*Math.floor(width/2))), (int)(width*levelPanelBoundingBox.width()), (int)((height * levelPanelBoundingBox.height()) - (levelPanelBoundingBox.height()*Math.floor(width/2))) + (int)(levelPanelBoundingBox.height()*Math.floor(width/2)));
					Rect rightEdge = new Rect(0, 0, (int)(levelPanelBoundingBox.width()*Math.floor(width/2)), (int)(height * levelPanelBoundingBox.height()));
					Rect leftEdge = new Rect((int)((width * levelPanelBoundingBox.width()) - (levelPanelBoundingBox.width()*Math.floor(width/2))), 0, (int)((width * levelPanelBoundingBox.width()) - (levelPanelBoundingBox.width()*Math.floor(width/2))) + (int)(levelPanelBoundingBox.width()*Math.floor(width/2)), (int)(height * levelPanelBoundingBox.height()));

					//Calculate the mouse difference.
					Point mouseDifference = new Point (mouseRotLocation.left - mousePosition.right, mouseRotLocation.top - mousePosition.top);
					
					//Change the rotation if the threshold has been met and the mouse started in the valid area.
					if(topEdge.contains(mousePosition) || bottomEdge.contains(mousePosition))
					{
						if(mouseDifference.x >= ROTATION_THRESHOLD)
						{
							if(topEdge.contains(mousePosition))
							{
								rotateRing(0, true);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
							else
							{
								rotateRing(0, false);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
						}
						else if(mouseDifference.x <= -ROTATION_THRESHOLD)
						{
							if(topEdge.contains(mousePosition))
							{
								rotateRing(0, false);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
							else
							{
								rotateRing(0, true);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
						}
					}
					if(rightEdge.contains(mousePosition) || leftEdge.contains(mousePosition))
					{
						if(mouseDifference.y <= -ROTATION_THRESHOLD)
						{
							if(rightEdge.contains(mousePosition))
							{
								rotateRing(0, true);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
							else
							{
								rotateRing(0, false);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
						}
						else if(mouseDifference.y >= ROTATION_THRESHOLD)
						{
							if(rightEdge.contains(mousePosition))
							{
								rotateRing(0, false);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
							else
							{
								rotateRing(0, true);
								mousePosition.offsetTo(mouseRotLocation.left, mouseRotLocation.top);
								mouseRotPosInit = false;
							}
						}
					}
				}
			
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				levelPanel.update();
			}
			if(doneRotating)
			{
				triggerHandler.triggerTest(new TilePlayer((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top)), this);
			}
			player.update();
		}
		updatingFinished = true;
		
		if(!playerDestination.equals((int)player.getBoundingBox().left, (int)player.getBoundingBox().top) && doneRotating)
		{
			player.moveTo(playerDestination.x, playerDestination.y);
			Log.d("Han", "Moving to dest.");
		}

		invalidate();
		}
		
		if(levelDone)
		{
			currentEngine.loadNextLevel();
		}
	}
	
	public void onDraw(Canvas canvas)
	{
		paintingFinished = false;
		
		canvas.drawRect(screenRect, paint);
		
		if(!loading)
		{
			update();
			
			for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
				levelPanel.draw(canvas);
			}
			
			if(doneRotating)
			{
				player.draw(canvas);
			}
			
	        if(levelRotation)
	        {
	        	//Draw the rotation vortex in the middle of the screen.
	        	//Note:This is assuming that all level panels are the same sizes.
	        	canvas.drawBitmap(ROTATION_VORTEX_SPRITE, (width*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getWidthInTiles())/2 - GlobalConstants.getTileSize()/2, (height*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getHeightInTiles())/2 - GlobalConstants.getTileSize()/2, null);
	        }
		}
		//Notify the app that there is a change in the game space.
		invalidate();
		paintingFinished = true;
	}

	//Find the tile that was left and activate it.(Might need to be redone.)
	public void activateOnExitTile(TilePlayer tempPlayerTile)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			if(levelPanel.getBoundingBoxArea().contains(tempPlayerTile.getBoundingBox()))
			{
				for(int tileNumber = 0;tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
				{
					BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
					if(baseTile.compare(tempPlayerTile) == 0)
					{
						baseTile.onExit(this);
					}
				}
			}
		}
	}
	
	//Find the tile that was entered and activate it.(Might need to be redone.)
	public void activateOnEnterTile(TilePlayer tempPlayerTile)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			if(levelPanel.getBoundingBoxArea().contains(tempPlayerTile.getBoundingBox()))
			{
				for(int tileNumber = 0;tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
				{
					BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
					if(baseTile.compare(tempPlayerTile) == 0)
					{
						baseTile.onEnter(this);
					}
				}
			}
		}
	}
	
	//Finds the rings and fill the list of rings so that we always have them.
	private void findRings()
	{
		//Create rectangels outlines that go along each ring possible in this level.
		//This list contains the outlines represented by four rectangles.  The list
		//does from outer ring to inner ring.
		ArrayList<ArrayList<Rect>> listOfOutlines = new ArrayList<ArrayList<Rect>>();
		
		//Grab an example level panel to get the dimensions.
		Rect levelPanelBoundingBox = new Rect(listOfLevelPanels.get(0).getBoundingBoxArea().left, listOfLevelPanels.get(0).getBoundingBoxArea().top, listOfLevelPanels.get(0).getBoundingBoxArea().right, listOfLevelPanels.get(0).getBoundingBoxArea().bottom);
		
		//Error check offset.  This is to make sure the outline only intersect the right level panels.
		int errorCheckOffset = 5;
		
		//A level can have floor(dimension/2) rings so make the rectangles
		//to represent that.
		for(int currentRingOutlineBeingMade = 0; currentRingOutlineBeingMade < Math.floor(width/2); currentRingOutlineBeingMade++)
		{
			//System.out.println("Ring # : " + currentRingOutlineBeingMade);
			Rect topEdge = new Rect((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset) + (int)(GlobalConstants.getLevelWidth() - (levelPanelBoundingBox.width()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset) + 1);
			Rect bottomEdge = new Rect((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset), (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.height() * (currentRingOutlineBeingMade)) - (errorCheckOffset * 2)), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset) + (int)(GlobalConstants.getLevelWidth() - (levelPanelBoundingBox.width()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)), (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.height() * (currentRingOutlineBeingMade)) - (errorCheckOffset * 2)) + 1);
			Rect leftEdge = new Rect((int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.width() + errorCheckOffset) + 1, (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset) + (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.height()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)));
			Rect rightEdge = new Rect((int)(GlobalConstants.getLevelWidth() - currentRingOutlineBeingMade * levelPanelBoundingBox.width() - errorCheckOffset), (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset), (int)(GlobalConstants.getLevelWidth() - currentRingOutlineBeingMade * levelPanelBoundingBox.width() - errorCheckOffset) + 1, (int)(currentRingOutlineBeingMade*levelPanelBoundingBox.height() + errorCheckOffset) + (int)(GlobalConstants.getLevelHeight() - (levelPanelBoundingBox.height()*(currentRingOutlineBeingMade + 1*currentRingOutlineBeingMade)) - (errorCheckOffset*2)));
			
			ArrayList<Rect> currentRingOutline = new ArrayList<Rect>();
			currentRingOutline.add(topEdge);
			currentRingOutline.add(bottomEdge);
			currentRingOutline.add(leftEdge);
			currentRingOutline.add(rightEdge);
			
			listOfOutlines.add(currentRingOutline);
			
			/*for(Rect rectOfCurrentRing : currentRingOutline)
			{
				Log.d("Han", "	X : " + rectOfCurrentRing.left);
				Log.d("Han","	Y : " + rectOfCurrentRing.top);
				Log.d("Han","	W : " + rectOfCurrentRing.width());
				Log.d("Han","	H : " + rectOfCurrentRing.height());
			}*/
		}

		//Go through all the panels and check to see which ring they belong to.
		
		for(int outlineNumber = 0; outlineNumber < listOfOutlines.size(); outlineNumber++)
		{
			//System.out.println("Outline # : " + outlineNumber);
			//int panelNumber = 0;
			for(int levelPanelNumber = 0; levelPanelNumber < listOfLevelPanels.size(); levelPanelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanels.get(levelPanelNumber);
				for(int rectNumber = 0;rectNumber < listOfOutlines.get(outlineNumber).size(); rectNumber++)
				{
					Rect outlineEdge = listOfOutlines.get(outlineNumber).get(rectNumber);
					//If the panel intersect the outline then it is part of that ring.
					if(levelPanel.getBoundingBoxArea().intersects(outlineEdge.left, outlineEdge.top, outlineEdge.right, outlineEdge.bottom))
					{
						//Do not add if it has already been added to that level panel.
						boolean levelPanelAlreadyAdded = false;
						for(int ringPanelNumber = 0; ringPanelNumber < listOfRings.get(outlineNumber).size(); ringPanelNumber++)
						{
							LevelPanel levelPanelInRing = listOfRings.get(outlineNumber).get(ringPanelNumber);
							if(levelPanelInRing.getBoundingBoxArea().left == levelPanel.getBoundingBoxArea().left && levelPanelInRing.getBoundingBoxArea().top == levelPanel.getBoundingBoxArea().top)
							{
								levelPanelAlreadyAdded = true;
							}
						}
						//Add it if it is new.
						if(!levelPanelAlreadyAdded)
						{
							//System.out.println("Panel added : " + panelNumber);
							listOfRings.get(outlineNumber).add(levelPanel);
						}
					}
				}
				//panelNumber++;
			}
		}
	}
	
	//Calls and rotates the ring set and in the direction set.  0 for
	//ring number means all possible rings are rotated.
	private void rotateRing(int ringNumber, boolean typeOfRotation)
	{
		boolean okToRotate = true;
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			if(!listOfLevelPanels.get(panelNumber).isDoneMoving())
			{
				okToRotate = false;
			}
		}
		if(okToRotate)
		{
			//Go through the array of rings and rotate them all.
			if(ringNumber == 0)
			{
				for(int ringNum = 0; ringNum < listOfRings.size(); ringNum++)
				{
					ArrayList<LevelPanel> ring = listOfRings.get(ringNum);
					ringNum++;
					rotate(ring, ringNum, typeOfRotation);
				}
			}
			//Rotate the single ring.
			else
			{
				rotate(listOfRings.get(ringNumber - 1), ringNumber, typeOfRotation);
			}
		}
	}
	
	//Rotates the board.  False is CCW and true is CW.
	//The array holds the panel of the ring that is given to this method.
	//The ring number is the count of rings.  Ring 1 is the most outer and
	//the number increases as the depth does.
	//Kind of messy.  Will have to find a simple equation to do this but for
	//now this will do.
	private void rotate(ArrayList<LevelPanel> listOfLevelPanelsToRotate, int ringNumber, boolean typeOfRotation)
	{
		//Rotate Level Planes CW.
		if(typeOfRotation)
		{
			//Make sure the player only moved once.
			boolean playerMoved = false;
			for(int panelNumber = 0; panelNumber < listOfLevelPanelsToRotate.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanelsToRotate.get(panelNumber);
				//Keep the old bounding box of the level to figure out where to move the player.
				//Used to clone here.  Might break
				Rect oldLevelPanelBox = (Rect) new Rect(levelPanel.getBoundingBoxArea());
				
				//If the Level Panel is not at the left/right edges and is the top edge of the ring 
				//then move it to the right.
				if(levelPanel.getBoundingBoxArea().left != (ringNumber - 1) * levelPanel.getBoundingBoxArea().width() && levelPanel.getBoundingBoxArea().left != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber && levelPanel.getBoundingBoxArea().top == (ringNumber - 1) * levelPanel.getBoundingBoxArea().height())
				{
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left + levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
				}
				//If it is the bottom edge of the ring.
				else if(levelPanel.getBoundingBoxArea().left != (ringNumber - 1) * levelPanel.getBoundingBoxArea().width() && levelPanel.getBoundingBoxArea().left != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber)
				{
					//Move it to the left.
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left - levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
				}
				//If they are on the right edge. 
				else if(levelPanel.getBoundingBoxArea().left == GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber)
				{
					//If the panel is at the bottom right corner.
					if(levelPanel.getBoundingBoxArea().top == GlobalConstants.getLevelHeight() - levelPanel.getBoundingBoxArea().height() * ringNumber)
					{
						//Move it to the left.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left - levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
					}
					//If not.
					else
					{
						//Move it down.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left), (int)(levelPanel.getBoundingBoxArea().top + levelPanel.getBoundingBoxArea().height()));
					}
				}
				//If they are on the left edge.
				else if(levelPanel.getBoundingBoxArea().left == (ringNumber - 1) * levelPanel.getBoundingBoxArea().width())
				{
					//If the panel is at the top right corner.
					if(levelPanel.getBoundingBoxArea().top == (ringNumber - 1) * levelPanel.getBoundingBoxArea().height())
					{
						//Move it to the right.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left + levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
					}
					//If not.
					else
					{
						//Move it up.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left), (int)(levelPanel.getBoundingBoxArea().top - levelPanel.getBoundingBoxArea().height()));
					}
				}
				//If the player is on this level panel.  Move it the same amount as the level panel.
				if(oldLevelPanelBox.contains(player.getBoundingBox()) && !playerMoved)
				{
					playerMoved = true;
					playerDestination.set((int)(player.getBoundingBox().left + (levelPanel.getDestination().x - oldLevelPanelBox.left)), (int)(player.getBoundingBox().top + (levelPanel.getDestination().y - oldLevelPanelBox.top)));
				}
			}
		}
		//Rotate Level Planes CCW.
		else
		{
			boolean playerMoved = false;
			for(int panelNumber = 0;panelNumber < listOfLevelPanelsToRotate.size(); panelNumber++)
			{
				LevelPanel levelPanel = listOfLevelPanelsToRotate.get(panelNumber);
				//Keep the old bounding box of the level to figure out where to move the player.
				//Used to clone here.  This might break.
				Rect oldLevelPanelBox = (Rect) new Rect(levelPanel.getBoundingBoxArea());
				
				//If the Level Panel is not at the left/right edges and is the top edge of the ring 
				//then move it to the left.
				if(levelPanel.getBoundingBoxArea().left != (ringNumber - 1) * levelPanel.getBoundingBoxArea().width() && levelPanel.getBoundingBoxArea().left != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber && levelPanel.getBoundingBoxArea().top == (ringNumber - 1) * levelPanel.getBoundingBoxArea().height())
				{
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left - levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
				}
				//If it is the bottom edge of the ring.
				else if(levelPanel.getBoundingBoxArea().left != (ringNumber - 1) * levelPanel.getBoundingBoxArea().width() && levelPanel.getBoundingBoxArea().left != GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber)
				{
					//Move it to the right.
					levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left + levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
				}
				//If they are on the right edge. 
				else if(levelPanel.getBoundingBoxArea().left == GlobalConstants.getLevelWidth() - levelPanel.getBoundingBoxArea().width() * ringNumber)
				{
					//If the panel is at the top right corner.
					if(levelPanel.getBoundingBoxArea().top == (ringNumber - 1) * levelPanel.getBoundingBoxArea().height())
					{
						//Move it to the left.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left - levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
					}
					//If not.
					else
					{
						//Move it up.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left), (int)(levelPanel.getBoundingBoxArea().top - levelPanel.getBoundingBoxArea().height()));
					}
				}
				//If they are on the left edge.
				else if(levelPanel.getBoundingBoxArea().left == (ringNumber - 1) * levelPanel.getBoundingBoxArea().width())
				{
					//If the panel is at the bottom left corner.
					if(levelPanel.getBoundingBoxArea().top == GlobalConstants.getLevelHeight() - levelPanel.getBoundingBoxArea().height() * ringNumber)
					{
						//Move it to the right.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left + levelPanel.getBoundingBoxArea().width()), (int)(levelPanel.getBoundingBoxArea().top));
					}
					//If not.
					else
					{
						//Move it down.
						levelPanel.moveTo((int)(levelPanel.getBoundingBoxArea().left), (int)(levelPanel.getBoundingBoxArea().top + levelPanel.getBoundingBoxArea().height()));
					}
				}
				//If the player is on this level panel.  Move it the same amount as the level panel.
				if(oldLevelPanelBox.contains(player.getBoundingBox()) && !playerMoved)
				{
					playerMoved = true;
					playerDestination.set((int)(player.getBoundingBox().left + (levelPanel.getDestination().x - oldLevelPanelBox.left)), (int)(player.getBoundingBox().top + (levelPanel.getDestination().y - oldLevelPanelBox.top)));
				}
			}
		}
	}

	//Finds the Point of the tile that is closest to the old point.
	//If the point is unreachable then return the old position.
	private Rect findTile(Rect oldPoint)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				if(baseTile.getBoundingBox().contains(oldPoint))
				{
					return new Rect(baseTile.getBoundingBox().left, baseTile.getBoundingBox().top, baseTile.getBoundingBox().left + GlobalConstants.getTileSize(), baseTile.getBoundingBox().top + GlobalConstants.getTileSize());
				}
			}
		}
		return new Rect(GlobalConstants.getNotANumber(), GlobalConstants.getNotANumber(), GlobalConstants.getNotANumber(), GlobalConstants.getNotANumber());
	}
	
	private Rect findClosestTile(Rect oldPoint)
	{
		Log.d("Han", "Old X: " + oldPoint.left + " Y: " + oldPoint.top);
		
		int playerXPosDiff = Math.abs(player.getBoundingBox().left - oldPoint.left);
		int playerYPosDiff = Math.abs(player.getBoundingBox().top - oldPoint.top);
		
		//If true then path in x dir.  if false path in y dir.
		boolean findXPath;
		
		if(playerXPosDiff > playerYPosDiff)
		{
			findXPath = true;
		}
		else
		{
			findXPath = false;
		}
		
		Rect closestRect = new Rect();
		int xDifference = 9999;
		int yDifference = 9999;
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				
				if(Math.abs(oldPoint.left - baseTile.getBoundingBox().left) + Math.abs(oldPoint.top - baseTile.getBoundingBox().top) < xDifference + yDifference)
				{
					if(findXPath && Math.abs(oldPoint.left - baseTile.getBoundingBox().left) < xDifference)
					{
						xDifference = Math.abs(oldPoint.left - baseTile.getBoundingBox().left);
						yDifference = Math.abs(oldPoint.top - baseTile.getBoundingBox().top);
						closestRect = new Rect(baseTile.getBoundingBox().left, baseTile.getBoundingBox().top, baseTile.getBoundingBox().right, baseTile.getBoundingBox().bottom);
						Log.d("Han", "New X: " + closestRect.left + " Y: " + closestRect.top);
					}
					else if(!findXPath && Math.abs(oldPoint.top - baseTile.getBoundingBox().top) < yDifference)
					{
						xDifference = Math.abs(oldPoint.left - baseTile.getBoundingBox().left);
						yDifference = Math.abs(oldPoint.top - baseTile.getBoundingBox().top);
						closestRect = new Rect(baseTile.getBoundingBox().left, baseTile.getBoundingBox().top, baseTile.getBoundingBox().right, baseTile.getBoundingBox().bottom);
						Log.d("Han", "New X: " + closestRect.left + " Y: " + closestRect.top);
					}
					
				}
			}
		}
		return closestRect;
	}
	
	//Find the actual tile in the location.
	public BaseTile getTileAt(Rect rect)
	{
		for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
		{
			LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				if(baseTile.getBoundingBox().contains(rect.left + 1, rect.top + 1))
				{
					return baseTile;
				}
			}
		}
		return new TilePlayer(GlobalConstants.getNotANumber(), GlobalConstants.getNotANumber());
	}
	
	//Grab input.
	public boolean onTouch(View view, MotionEvent event) 
	{
		switch(event.getAction())
		{
			//When finger is down.
			case MotionEvent.ACTION_DOWN:
			{
				if(!loading)
				{
					//Set the temp boolean and grab the mouse location.
					//The actual boolean is not set because this mouse listener runs
					//on a separate thread.
					boolean mouseHeldTemp = true;
					
					if(!levelRotation)
					{
						mousePosition.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
					}
					else if(levelRotation && !mouseRotPosInit)
					{
						mousePosition.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
						mouseRotLocation.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
						mouseRotPosInit = true;
					}
										
					//If the the game is in the level rotate state then any clicks in the
					//middle will reset the board.
					Rect middleRect = new Rect((GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getWidthInTiles()) - GlobalConstants.getTileSize()/2 , (GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getHeightInTiles()) - GlobalConstants.getTileSize()/2, (GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getWidthInTiles()) - GlobalConstants.getTileSize()/2 + GlobalConstants.getTileSize(), GlobalConstants.getTileSize() +(height*GlobalConstants.getTileSize()*listOfLevelPanels.get(0).getHeightInTiles()) - GlobalConstants.getTileSize()/2);
					if(levelRotation && middleRect.contains(mousePosition.left, mousePosition.top))
					{
						levelRotation = false;
						mouseHeldTemp = false;
						mouseRotPosInit = false;
					}
						
					if(findTile(mousePosition).intersects(player.getBoundingBox().left, player.getBoundingBox().top, player.getBoundingBox().right, player.getBoundingBox().bottom) && findClosestTile(mousePosition).left != GlobalConstants.getNotANumber())
					{
						levelRotation = true;
						mouseHeldTemp = true;
						//The last found path is now invalid.
						path.clear();
					}
					
					boolean offScreen = true;
					for(int panelNumber = 0; panelNumber < listOfLevelPanels.size(); panelNumber++)
					{
						LevelPanel levelPanel = listOfLevelPanels.get(panelNumber);
						if(levelPanel.getBoundingBoxArea().contains(mousePosition))
						{
							offScreen = false;
						}
					}
					
					if(!levelRotation)
					{
						if(offScreen)
						{
							mousePosition = findClosestTile(mousePosition);
						}
						else
						{
							mousePosition = findTile(mousePosition);
						}
					}
					
					mouseHeld = mouseHeldTemp;
				}
				return true;
			}
			case MotionEvent.ACTION_MOVE:
			{
				if(!loading)
				{
					if(!levelRotation)
					{
						mousePosition.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
					}
					else if(levelRotation && !mouseRotPosInit)
					{
						mousePosition.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
						mouseRotPosInit = true;
					}
					
					mouseRotLocation.set((int)(event.getX()), (int)(event.getY()), (int)(event.getX()) + 1, (int)(event.getY()) + 1);
					mouseHeld = true;
				}
				return true;
			}
			case MotionEvent.ACTION_UP:
			{
				if(!loading && doneRotating)
				{
					mouseRotPosInit = false;
					mouseHeld = false;
					path.clear();
				}
				return true;
			}
		}
		return false;
	}
	
		
	public TilePlayer getPreviousPlayerTile()
	{
		return previousPlayerTile;
	}
	
	
	public void movePlayer(int newXPosition, int newYPosition)
	{
		previousPlayerTile.moveTo((int)(player.getBoundingBox().left), (int)(player.getBoundingBox().top));
		activateOnExitTile(previousPlayerTile);
		player.moveTo(newXPosition, newYPosition);
		playerDestination.set(newXPosition, newYPosition);
		activateOnEnterTile(new TilePlayer(newXPosition, newYPosition));
	}

	public Player getPlayer() 
	{
		return player;
	}
	
	public int getLevelDimensions()
	{
		return GlobalConstants.getTileSize() * width * listOfLevelPanels.get(0).getWidthInTiles();
	}
	
	public void createRings()
	{
		for(int ringPlaceholder = 0; ringPlaceholder < Math.floor(width/2); ringPlaceholder++)
		{
			listOfRings.add(new ArrayList<LevelPanel>());
		}
		findRings();
	}
	
	public void addTrigger(BaseTile trigger, ArrayList<BaseTile> listOfTriggeredTiles)
	{
		triggerHandler.addNewTrigger(trigger, listOfTriggeredTiles);
		
		ArrayList<Rect> newListOfTriggers = new ArrayList<Rect>();
		
		newListOfTriggers.add(trigger.getBoundingBox());
		for(int rectNumber = 0; rectNumber < listOfTriggeredTiles.size(); rectNumber++)
		{
			newListOfTriggers.add(listOfTriggeredTiles.get(rectNumber).getBoundingBox());
		}
		
		savedTriggers.add(newListOfTriggers);
	}
	
	public ArrayList<LevelPanel> getListOfLevelPanels()
	{
		return listOfLevelPanels;
	}

	public void setSpawn(int x, int y) 
	{
		playerSpawn.set(x, y);	
	}
	
	public void levelDone()
	{
		levelDone = true;
	}
	
	public boolean isLevelDone()
	{
		return levelDone;
	}
	
	public void loadNextLevel()
	{
		engine.loadNextLevel();
	}
	
	public void clearPath()
	{
		mouseHeld = false;
		path.clear();
	}
	
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
	{
	    this.setMeasuredDimension(getLevelDimensions(), getLevelDimensions());
	}
	
	public void setEngine(Engine engine)
	{
		currentEngine = engine;
	}
}
