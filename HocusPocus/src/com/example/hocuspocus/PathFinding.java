package com.example.hocuspocus;

import java.util.ArrayList;

import android.util.Log;
import hocusPocusTesterPackage.Tiles.BaseTile;

//The Pathfinding class takes in the Level Panels and creates a path of tiles
//from the start to the destination.
//Note:Ignore the prints.  I left them there in case this needs to be debugged in the future.
public class PathFinding 
{
	//The passed down level panels.
	private ArrayList<LevelPanel> levelPanels;
	//The nodes generated from the level panels.
	private ArrayList<Node> levelTileNodes;
	//The dimensions in tiles of the level.
	private int levelWidth;
	
	//The lists that help with the search.
	//List that contains Nodes that are being considered to be along the path. 
	ArrayList<Node> openList;
	//List that contains Nodes that are done being considerd.
	ArrayList<Node> closedList;
	
	//Width and height are in tile units.  The width and the height of the level.
	public PathFinding(int width , int height, ArrayList<LevelPanel> levelPanelsGive)
	{
		openList = new ArrayList<Node>();
		closedList = new ArrayList<Node>();
		
		levelWidth = width;
		
		levelTileNodes = new ArrayList<Node>();
		
		//Fill the array list with empty nodes so they can reset later to the correct nodes.
		for(int currentNumberBeingAdded = 0; currentNumberBeingAdded < width*height; currentNumberBeingAdded++)
		{
			levelTileNodes.add(new Node());
		}
		
		//Grab the level panels that make up the level.
		levelPanels = new ArrayList<LevelPanel>();
		levelPanels = levelPanelsGive;
		
		int xPositionInLevel = 0;
		int yPositionInLevel = 0;
		//Go through the level panels and copy the contents to the right location in the
		//nodes array list.
		for(int levelPanelNumber = 0; levelPanelNumber < levelPanels.size(); levelPanelNumber++)
		{
			LevelPanel levelPanel = levelPanels.get(levelPanelNumber);
			//Grab the location of the level panel in terms of tiles.
			xPositionInLevel = (int)(levelPanel.getBoundingBoxArea().left/GlobalConstants.getTileSize());
			yPositionInLevel = (int)(levelPanel.getBoundingBoxArea().top/GlobalConstants.getTileSize());
			
			for(int tileNumber = 0; tileNumber < levelPanel.getListOfTiles().size(); tileNumber++)
			{
				BaseTile baseTile = levelPanel.getListOfTiles().get(tileNumber);
				//Create a new node with this tile.
				Node newNode = new Node();
				newNode.tile = baseTile;
				
				//System.out.println("Current tile: " + (xPositionInLevel + yPositionInLevel*(levelWidth)));
				//Calculate the neighbour tile location.
				for(int xDiffPosition = -1; xDiffPosition <= 1; xDiffPosition++)
				{
					for(int yDiffPosition = -1; yDiffPosition <= 1; yDiffPosition++)
					{
						//System.out.println("Considering: " + ((xPositionInLevel + yPositionInLevel*(levelWidth)) + xDiffPosition + (levelWidth * yDiffPosition)));
						//Make sure it is not the same tile.  Make sure no tiles that go across edges are considered.
						if(!(xDiffPosition == 0 && yDiffPosition == 0) && (xDiffPosition == 0 || yDiffPosition == 0) && !((xDiffPosition == -1 && ((xPositionInLevel + yPositionInLevel*(levelWidth)) % levelWidth == 0))) && !(xDiffPosition == 1 && ((xPositionInLevel + yPositionInLevel*(levelWidth)) + 1)%(levelPanel.getWidthInTiles()*(width/levelPanel.getWidthInTiles())) == 0))
						{
							if(!(xDiffPosition == -1 && ((xPositionInLevel + yPositionInLevel*(levelWidth)) == 0)))
							{
								if(((xPositionInLevel + yPositionInLevel*(levelWidth)) + xDiffPosition + (levelWidth * yDiffPosition)) >= 0 && ((xPositionInLevel + yPositionInLevel*(levelWidth)) + xDiffPosition + (levelWidth * yDiffPosition)) <= width*height - 1)
								{
									//If the neighbour is in a valid location then add it.
									//System.out.println("Adding Neighbour: " + ((xPositionInLevel + yPositionInLevel*(levelWidth)) + xDiffPosition + (levelWidth * yDiffPosition)));
									newNode.neighbours.add(((xPositionInLevel + yPositionInLevel*(levelWidth)) + xDiffPosition + (levelWidth * yDiffPosition)));
								}
							}
						}
					}
				}
				//System.out.println("X tile: " + xPositionInLevel);
				//System.out.println("Y tile: " + yPositionInLevel);
				//System.out.println("Level Width: " + levelWidth);
				//System.out.println("Replacing: " + xPositionInLevel + yPositionInLevel*(levelWidth));
				
				//Set the tile to the right position.
				levelTileNodes.set(xPositionInLevel + yPositionInLevel*(levelWidth), newNode);
				
				//Increment the position and reset them if they cross edges.
				xPositionInLevel++;
				//If the number of tiles is past the number of tiles that this level panel is composed of.
				if(xPositionInLevel >= levelPanel.getWidthInTiles() + (int)(levelPanel.getBoundingBoxArea().left/GlobalConstants.getTileSize()))
				{
					yPositionInLevel++;
					xPositionInLevel = (int)(levelPanel.getBoundingBoxArea().left/GlobalConstants.getTileSize());
				}
			}
		}
		
		//Print out the tiles as the PathFinding sees it.
		//System.out.println("Created Path Finder.");
		/*int number = 0;
		for(int i = 0; i < levelTileNodes.size(); i++)
		{
			number++;
			if(number%6 == 0)
			{
				if(levelTileNodes.get(i).tile.isWalkable())
				{
					System.out.println("O");
				}
				else
				{
					System.out.println("X");
				}
			}
			else
			{
				if(levelTileNodes.get(i).tile.isWalkable())
				{
					System.out.print("O");
				}
				else
				{
					System.out.print("X");
				}
			}
		}*/
	}
	
	//This will calculate the best guess on how many Nodes it will take
	//to get to the destination from the start.  This is the Manhattan 
	//distance method(count number Nodes only horizontally and vertically
	//and ignore the obstacles).
	private int calculateHScore(BaseTile startTile, BaseTile endTile)
	{
		int manhattanGuess = 0;
		
		manhattanGuess += Math.abs((int)(startTile.getBoundingBox().left - endTile.getBoundingBox().left));
		manhattanGuess += Math.abs((int)(startTile.getBoundingBox().top - endTile.getBoundingBox().top));
		
		return manhattanGuess;
	}
	
	//Returns an array list with a path to the destination tile.  The
	//array begins at the end and ends at the beginning.  The array 
	//returned will be empty if no path can be found.
	public ArrayList<BaseTile> findPath(BaseTile start, BaseTile destination)
	{
		//System.out.println("Finding Path.");
		ArrayList<BaseTile> path = new ArrayList<BaseTile>();
		
		Node startNode = new Node();
		
		//Find the start node tile.
		for(int nodeNumber = 0;nodeNumber < levelTileNodes.size(); nodeNumber++)
		{
			Node findStartNode = levelTileNodes.get(nodeNumber);
			if(start.compare(findStartNode.tile) == 0)
			{
				startNode = findStartNode;
				//System.out.println("Found start node: " + startNode.tile.getBoundingBox().getX() + " " + startNode.tile.getBoundingBox().getY());
			}
		}
		
		//Add the start Node to the closed list.
		closedList.add(startNode);
		
		//Add all the neighbours of the start node to the open list.
		for(int neighNumber = 0;neighNumber < startNode.neighbours.size(); neighNumber++)
		{
			Integer neighbourNumber = startNode.neighbours.get(neighNumber);
			//Calculate the path score for the nodes.
			Node neighbourNode = levelTileNodes.get(neighbourNumber);
			neighbourNode.gScore = 1;
			neighbourNode.hScore = calculateHScore(neighbourNode.tile, destination);
			neighbourNode.fScore = neighbourNode.gScore + neighbourNode.hScore;
			openList.add(neighbourNode);
			//System.out.println("Adding neighbour: " + neighbourNode.tile.getBoundingBox().getX() + " " + neighbourNode.tile.getBoundingBox().getY());
		}
		
		//Continue the meat of the search until we looked at all potential nodes.
		while(!openList.isEmpty())
		{
			//System.out.println("Number in open list: " + openList.size());
			//Find the lowest fScore in the open list.
			int currentLowest = Integer.MAX_VALUE;
			int intOfCurrentNodeBeingProcessed = 0;
			for(int lowestNodeInOpenList = 0; lowestNodeInOpenList < openList.size(); lowestNodeInOpenList++)
			{
				if(openList.get(lowestNodeInOpenList).fScore < currentLowest)
				{
					currentLowest = openList.get(lowestNodeInOpenList).fScore;
					intOfCurrentNodeBeingProcessed = lowestNodeInOpenList;
				}
			}
			
			//Remove the Node from the open list and process it.
			Node currentNodeBeingProcessed = openList.get(intOfCurrentNodeBeingProcessed);
			
			if(currentNodeBeingProcessed.tile.isWalkable())
			{
				//Remove it from the open list and add it to the closed list.
				openList.remove(intOfCurrentNodeBeingProcessed);
				closedList.add(currentNodeBeingProcessed);
				
				//If this is the destination then return the path.
				if(currentNodeBeingProcessed.tile.compare(destination) == 0)
				{
					//Return the path with the start tile at the end.
					//Add the destination last.
					path.add(destination);
					int reverseClosedList = 2;
					//Grab the last item in the closed list(besides this node) and reverse it to get
					//the path.
					Node nodeToReturn = closedList.get(closedList.size() - reverseClosedList);
					while(nodeToReturn != null)
					{
						path.add(nodeToReturn.tile);
						
						nodeToReturn = nodeToReturn.parentNode;
					}
					//System.out.println("Return path.");
					return path;
				}
				
				//If not process the neighbours of the picked Node.
				for(int processNodeNeigh = 0; processNodeNeigh < currentNodeBeingProcessed.neighbours.size(); processNodeNeigh++)
				{
					Integer neighbourNumber = currentNodeBeingProcessed.neighbours.get(processNodeNeigh);
					if(levelTileNodes.get(neighbourNumber).tile.isWalkable())
					{
						//Check to see if the node has been processed already.
						//We do not process it again if it has been processed already.
						boolean alreadyProccessed = false;
						for(int closeListNumber = 0; closeListNumber < closedList.size(); closeListNumber++)
						{
							Node nodeProcessedAlready = closedList.get(closeListNumber);
							if(nodeProcessedAlready.tile.compare(levelTileNodes.get(neighbourNumber).tile) == 0)
							{
								alreadyProccessed = true;
							}
						}
						
						//If it has not been processed we check to see if it is in the open list
						//or not.
						if(!alreadyProccessed)
						{
							//Check to see if we came across this node before.
							boolean inOpenList = false;
							int placeInOpenList = 0;
							for(int nodeNumberInOpenList = 0; nodeNumberInOpenList < openList.size(); nodeNumberInOpenList++)
							{
								Node nodeProcessedAlready = openList.get(nodeNumberInOpenList);
								if(nodeProcessedAlready.tile.compare(levelTileNodes.get(neighbourNumber).tile) == 0)
								{
									inOpenList = true;
									placeInOpenList = nodeNumberInOpenList;
								}
							}
							//If it is already in the open list then update the values.
							if(inOpenList)
							{
								Node updatedNode = openList.get(placeInOpenList);
								updatedNode.gScore = currentNodeBeingProcessed.gScore + 1;
								updatedNode.hScore = calculateHScore(updatedNode.tile, destination);
								updatedNode.fScore = updatedNode.gScore + updatedNode.hScore;
								if(updatedNode.fScore < openList.get(placeInOpenList).fScore)
								{
									updatedNode.parentNode = currentNodeBeingProcessed;
									closedList.set(placeInOpenList, updatedNode);
								}
							}
							//Else add it to the open list.
							else
							{
								Node newNode = levelTileNodes.get(neighbourNumber);
								//This new Node is 1 more away.
								newNode.gScore = currentNodeBeingProcessed.gScore + 1;
								newNode.hScore = calculateHScore(newNode.tile, destination);
								newNode.fScore = newNode.gScore + newNode.hScore;
								newNode.parentNode = currentNodeBeingProcessed;
								openList.add(newNode);
							}
						}
					}
				}
			}
			else
			{
				//If it is not walkable then remove it.
				openList.remove(intOfCurrentNodeBeingProcessed);
			}
		}
		//Return an empty path.
		//System.out.println("No path found.");
		return path;
	}
}

//Nodes hold a tile and the distance.  This class is
//like a struct.
class Node
{
	//The neighbours as indexes of the array list of nodes.
	public ArrayList<Integer> neighbours;
	public BaseTile tile;
	//The number of Nodes to get to this Node from the start Node.
	public int gScore;
	//The number of Nodes to get from this Node to the end Node.  A best guess.
	public int hScore;
	//The best guess to get from the start Node to the end Node.
	public int fScore;
	public Node parentNode;
	
	public Node()
	{
		neighbours = new ArrayList<Integer>();
		parentNode = null;
	}
}
