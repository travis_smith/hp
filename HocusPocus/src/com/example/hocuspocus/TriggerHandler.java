package com.example.hocuspocus;

import hocusPocusTesterPackage.Tiles.BaseTile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

//The trigger handler class takes care of triggers.  It holds a space on the level
//and when the space is tiggered it calls the proper onTrigger functions of the
//associated tiles.
public class TriggerHandler 
{
	//Maps the space to the list of tiles to activate once it is triggered.
	private HashMap<BaseTile, ArrayList<BaseTile>> triggerMap;
	//A list of already triggered tiles.  will be used to make sure tirggers only fire
	//once.
	private BaseTile currentlyFiredTile;
	
	public TriggerHandler()
	{
		triggerMap = new HashMap<BaseTile, ArrayList<BaseTile>>();
	}
	
	//Add a new list of triggers with an associated space.
	public void addNewTrigger(BaseTile triggerSpace, ArrayList<BaseTile> listOfTiggeredTiles)
	{
		triggerMap.put(triggerSpace, listOfTiggeredTiles);
	}
	
	//Pass a Rect to this method and it will trigger the correct trigger if any.
	public void triggerTest(BaseTile triggerTripper, Level level)
	{		
		Iterator<Entry<BaseTile, ArrayList<BaseTile>>> triggerMapIterator = triggerMap.entrySet().iterator();
	    while (triggerMapIterator.hasNext()) 
	    {
	    	Map.Entry<BaseTile, ArrayList<BaseTile>> pairs = (Map.Entry<BaseTile, ArrayList<BaseTile>>)triggerMapIterator.next();
	    	if(triggerTripper.getBoundingBox().intersects(((BaseTile)pairs.getKey()).getBoundingBox().left, ((BaseTile)pairs.getKey()).getBoundingBox().top, ((BaseTile)pairs.getKey()).getBoundingBox().right, ((BaseTile)pairs.getKey()).getBoundingBox().bottom) && currentlyFiredTile.compare(triggerTripper) != 0)
	    	{
	    		ArrayList<BaseTile> listOfTrippedTriggers = (ArrayList<BaseTile>)pairs.getValue();
	    		for(int triggeredTileNumber = 0; triggeredTileNumber < listOfTrippedTriggers.size(); triggeredTileNumber++)
	    		{	
	    			listOfTrippedTriggers.get(triggeredTileNumber).onTriggerEnter(level);
	    		}
	    	}
	    }
	    currentlyFiredTile = triggerTripper;
	}
	
	public void reset()
	{
		triggerMap.clear();
	}
}
