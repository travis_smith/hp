package com.example.hocuspocus;

import java.util.ArrayList;

import fileIO.FileIO;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
//import fileIO.FileIO;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

//The engine class is responsible for the higher level game processes.
//Its purpose is to allow the Level class to just have to take care
//of game logic and rendering.  The Engine class loads levels and takes
//care of the higher level of execution.
public class Engine extends Activity
{
	//Checks if the Engine is running or not.
	private boolean running;
	
	//The level that is being tested.
	private Level currentLevel;
	
	private double timeSinceLastUpdate;
	private double timeAtLastUpdate;
	
	//For now hardcode the level progression as a arraylist.
	//The levels will loop to the start once it gets to the end.
	private ArrayList<String> listOfLevelNames;
	//The current level.
	private int currentLevelNumber;
	
	//The audio player.
	AudioPlayer audioPlayer;
	
	 public void onCreate(Bundle savedInstanceState) 
	 {		
		  //Set up the window.
		  super.onCreate(savedInstanceState);
		  requestWindowFeature(Window.FEATURE_NO_TITLE);
		  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		  
		  //Add in the levels.  Hardcoded.
		  listOfLevelNames = new ArrayList<String>();
		  listOfLevelNames.add("HanTestNew");
		  listOfLevelNames.add("HanTestNew2");
		  currentLevelNumber = 0;
		   
		  //Set up the level and the view and add it to the layout.
		  createEngine();
		  RelativeLayout layout = new RelativeLayout(this);
		  layout.setBackgroundColor(Color.MAGENTA);
		  layout.setBackgroundResource(R.drawable.background);
		  RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		  params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		  layout.addView(currentLevel, params);
		  
		  //Create the reset button and add it to the view.
		  Button resetButton = new Button(this);
		  resetButton.setText("Reset Level");
		  resetButton.setOnClickListener(new View.OnClickListener() 
		  {
				public void onClick(View v) 
				{
				    currentLevel.reloadLevel();
				}
	      });
		  layout.addView(resetButton);
		  setContentView(layout);
		  
		  //Add an overlay view that will pass down touch from the whole screen.
		  View overlayView = new View(this);
		  overlayView.setOnTouchListener(new OnTouchListener() {
		      public boolean onTouch(View view, MotionEvent event) 
		      {
		    	switch(event.getAction())
		  		{
		  			//When finger is down.
		  			case MotionEvent.ACTION_DOWN:
		  			{
		  				currentLevel.onTouch(currentLevel, event);
		  				return false;
		  			}
		  		}
		          return false;
		      }
		  });
		  layout.addView(overlayView);
		  
		  audioPlayer = new AudioPlayer(this);
		  //Uncomment to play music.
		  audioPlayer.playBackground(this);
	 }

	//Creates the engine and starts it.  This will prompt the
	//updating and rendering of the game.
	public void createEngine()
	{
		//Load the level.
		//Load level 3 for now.
		currentLevel = new LevelLoader().loadLevel(this, this, currentLevel, FileIO.loadLevel(this, listOfLevelNames.get(0)));
		currentLevel.setEngine(this);
		currentLevel.invalidate();
		
		//Printing the tiles.
		/*for(int i = 0; i < currentLevel.getListOfLevelPanels().size(); i++)
		{
			for(int x = 0; x < currentLevel.getListOfLevelPanels().get(i).getListOfTiles().size(); x++)
			{
				Log.d("Han", "Tile: " + currentLevel.getListOfLevelPanels().get(i).getListOfTiles().get(x).getClass().toString());
			}
		}*/
		
		//Start the engine running.
		running = true;
		
		timeSinceLastUpdate = 0;
		//Create the board that the game will display on.
		//Create the display to hold the game based on the level.
		//Set the level dimensions and create the frame.
		GlobalConstants.setLevelWidth(currentLevel.getLevelDimensions());
		GlobalConstants.setLevelHeight(currentLevel.getLevelDimensions());
		LayoutParams levelLayout = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		currentLevel.setLayoutParams(levelLayout);
		//After all the variables are set find the rings for the level.
		currentLevel.createRings();
		
		timeAtLastUpdate = System.currentTimeMillis();
	}
		
	//Update the level.  Not using this for now.
	//Updating while drawing so need to fix to use this instead.  TODO
	public void update()
	{
		while(running)
		{
			timeSinceLastUpdate += System.currentTimeMillis() - timeAtLastUpdate;
			timeAtLastUpdate = System.currentTimeMillis();
			if(timeSinceLastUpdate >= GlobalConstants.getCycleRate())
			{
				timeSinceLastUpdate = 0;
				currentLevel.update();
			}
		}
	}
	
	public void loadNextLevel()
	{
		currentLevelNumber++;
		if(currentLevelNumber >= listOfLevelNames.size())
		{
			currentLevelNumber = 0;
		}
		
		new LevelLoader().reloadLevel(currentLevel, FileIO.loadLevel(this, listOfLevelNames.get(currentLevelNumber)));
		currentLevel.invalidate();
		
		GlobalConstants.setLevelWidth(currentLevel.getLevelDimensions());
		GlobalConstants.setLevelHeight(currentLevel.getLevelDimensions());
		
		currentLevel.createRings();
	}
	
	public void onStart()
	{
		super.onStart();
	}
	
	//Is the engine still running.
	public boolean isRunning()
	{
		return running;
	}
	
	protected void onDestroy() 
 	{
		super.onDestroy();
		audioPlayer.cleanUp();
	}
	
	protected void onStop() 
	{
		super.onStop();
	}
}
