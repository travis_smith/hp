package com.example.hocuspocus;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

public class AudioPlayer 
{
	AudioManager audioManager;
	
	//Use this to play short clips.  Only has
	//space for 1 MB so unload/load as necessary.
	SoundPool soundpool;
	
	//Use this to play large sounds.
	MediaPlayer mediaPlayer;
	
	//Number of channels depends on number of clips.
	final static int NUMBER_OF_CLIPS_TO_PLAY = 0;

	public AudioPlayer(Context context) 
	{
		//Create the audio manager.
		audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		
		//Create the soundpool.
		soundpool = new SoundPool(NUMBER_OF_CLIPS_TO_PLAY, AudioManager.STREAM_MUSIC, 0);
		
		//Create the media player and load the background music.
		mediaPlayer = MediaPlayer.create(context, R.raw.hocuspocusconceptone);
	}
	
	public void playBackground(Context context)
	{	
		//mediaPlayer.start();
	}
	
	//Clean the Media Player.
	public void cleanUp()
	{
		mediaPlayer.release();
		mediaPlayer = null;
	}
}
