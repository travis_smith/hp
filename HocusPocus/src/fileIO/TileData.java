package fileIO;

public class TileData 
{
	private char type;
	private int numParams;
	private int params[];
	
	protected TileData()
	{
		this.type = '0';
		this.numParams = 2;
		this.params = new int[]{0, 1};
	}
	
	protected TileData(char type, int numParams, int params[])
	{
		this.type = type;
		this.numParams = numParams;
		this.params = params;
	}
	
	protected void writeOut()
	{
		System.out.println("Type: " + type);
		System.out.println("Numer of Parameters: " + numParams);
		
		for(int index = 0; index < numParams; index++)
		{
			System.out.println("Parameter " + (index + 1) + ": " + params[index]);
		}
	}
	
	public char getType()
	{
		return type;
	}
	
	public int getNumParams()
	{
		return numParams;
	}
	
	public int[] getParams()
	{
		return params;
	}
}
