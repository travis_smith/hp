package fileIO;

public class Level
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Rotation Constants
	public static final int WHEEL = 0;
	public static final int GROUP = 1;
	public static final int SINGLE = 2;

	// Sizes
	private int numLayers;
	private int levelDimension;
	private int sectionDimension;
	
	// Level Data
	private String levelName;
	private int rotationType;
	private int numTeleporters;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================

	public Level(String name, int numLayers, int levelDimension, int sectionDimension, int rType, int numTeleporters)
	{
		// Initialize Main Data
		this.levelName = name;
		this.numLayers = numLayers;
		this.levelDimension = levelDimension;
		this.sectionDimension = sectionDimension;
		this.rotationType = rType;
		this.numTeleporters = numTeleporters;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	// Getters
	public String getLevelName()
	{
		return levelName;
	}
	public int getNumLayers()
	{
		return numLayers;
	}
	public int getLevelDimension()
	{
		return levelDimension;
	}
	public int getSectionDimension()
	{
		return sectionDimension;
	}
	public int getRotationType()
	{
		return rotationType;
	}
	public int getNumTeleporters() 
	{
		return numTeleporters;
	}
}
