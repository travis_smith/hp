package fileIO;

public class LevelData 
{
	private SectionData sections[][];
	private int lvlDimension;
	private int secDimension;
	private String name;
	private int rType;
	
	protected LevelData(int lvlDimension, int secDimension)
	{
		sections = new SectionData[lvlDimension][lvlDimension];
		
		for(int rIndex = 0; rIndex < lvlDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < lvlDimension; cIndex++)
			{
				sections[cIndex][rIndex] = new SectionData(secDimension);
			}	
		}
		
		this.lvlDimension = lvlDimension;
		this.secDimension = secDimension;
		this.name = "Test Level";
		this.rType = 1;
	}
	
	protected LevelData(String name, int rType, int lvlDimension, int secDimension, SectionData sections[][])
	{
		this.name = name;
		this.rType = rType;
		this.lvlDimension = lvlDimension;
		this.secDimension = secDimension;
		this.sections = sections;
	}
	
	protected void writeOut()
	{
		// Write Data
		System.out.println("Name: " + name);
		System.out.println("R-Type: " + rType);
		System.out.println("Level Dimension: " + lvlDimension);
		System.out.println("Section Dimension: " + secDimension);
		
		for(int rIndex = 0; rIndex < lvlDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < lvlDimension; cIndex++)
			{
				System.out.println("Section (" + cIndex + ", " + rIndex + ")");
				sections[cIndex][rIndex].writeOut();
			}
		}
		
	}
	
	public int getLvlDime()
	{
		return lvlDimension;
	}
	
	public int getSecDime()
	{
		return secDimension;
	}
	
	public SectionData[][] getSections()
	{
		return sections;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getRType()
	{
		return rType;
	}
}
