package fileIO;

public class SectionData 
{
	private TileData tileGrid[][];
	private int dimension;
	
	protected SectionData(int dimension)
	{
		this.dimension = dimension;
		
		tileGrid = new TileData[dimension][dimension];
		
		for(int rIndex = 0; rIndex < dimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < dimension; cIndex++)
			{
				tileGrid[cIndex][rIndex] = new TileData();
			}	
		}
	}
	
	protected SectionData(int dimension, TileData tileGrid[][])
	{
		this.dimension = dimension;
		this.tileGrid = tileGrid;
	}
	
	protected void writeOut()
	{
		for(int rIndex = 0; rIndex < dimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < dimension; cIndex++)
			{
				System.out.println("Tile (" + cIndex + ", " + rIndex + ")");
				tileGrid[cIndex][rIndex].writeOut();
			}
		}		
	}
	
	public TileData[][] getTileGrid()
	{
		return tileGrid;
	}
	
	public int getDimension()
	{
		return dimension;
	}
}
