package fileIO;

public class Tile 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Number of Types
	public static final int NUM_TYPES = 8;	// Needs to be incremented as types are added
	
	// Basic Tile Type Constants
	public static final char WALL_TILE = 'a';
	public static final char START_TILE = 'b';
	public static final char KEY_TILE = 'c';
	public static final char EXIT_TILE = 'd';
	public static final char FLOOR_TILE = 'e';
	public static final char ICE_TILE = 'f';
	
	// Special Tile Type Constants
	public static final char TELE_TILE = 'A';
	public static final char STAIRS_TILE = 'B';
	
	// Core Data
	private char type;
	private int numParameters;
	private int parameters[];
		
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public Tile(char type, int numParams, int params[])
	{
		this.type = type;
		this.numParameters = numParams;
		this.parameters = params;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	// Getters
	public char getType()
	{
		return type;
	}
	
	public int getNumParameters()
	{
		return numParameters;
	}
	
	public int[] getParameters()
	{
		return parameters;
	}
}
