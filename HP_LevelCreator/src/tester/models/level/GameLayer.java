package tester.models.level;

import gui.tilepops.BasicOptions;

import java.awt.Point;

import tester.models.Location;
import tester.models.Player;
import levelData.Tile;

/**
The GameLayer will encapsulate all tiles, and panels belonging to this layer. The layer will have
an entirely protected interface, all interaction with this class should be done by the GameLevel 
class. The GameLayer will contain the methods needed to rotate the levels panels.

@author 	Travis Smith
@version 	1.0 | 14 May 2014
*/
public class GameLayer
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size
	private int levelDim;
	private int sectionDim;
	
	// Start
	private Point startLocation;
	private boolean hasStart;
	
	// Panels
	private GamePanel panels[][];
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected GameLayer(int sectionDim, int levelDim, GameTile tiles[][])
	{
		this.levelDim = levelDim;
		this.sectionDim = sectionDim;
		this.startLocation = null;
		this.hasStart = false;
		
		panels = new GamePanel[levelDim][levelDim];
		
		// Split tiles and create panels
		for(int rIndex = 0; rIndex < levelDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < levelDim; cIndex++)
			{
				// Get Separate Tile Array
				GameTile panelTiles[][] = new GameTile[sectionDim][sectionDim];
				
				for(int tileRIndex = 0; tileRIndex < sectionDim; tileRIndex++)
				{
					for(int tileCIndex = 0; tileCIndex < sectionDim; tileCIndex++)
					{
						int mainArrayCol = (sectionDim * cIndex) + tileCIndex;
						int mainArrayRow = (sectionDim * rIndex) + tileRIndex;
						
						if(tiles[mainArrayCol][mainArrayRow].getType() == Tile.START_TILE)
						{
							hasStart = true;
							startLocation = new Point(mainArrayCol, mainArrayRow);
						}
						
						panelTiles[tileCIndex][tileRIndex] = tiles[mainArrayCol][mainArrayRow];
						
						panels[cIndex][rIndex] = new GamePanel(sectionDim, panelTiles);
					}
				}
			}
		}
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected boolean hasStart()
	{
		return hasStart;
	}
	
	protected Point getStartLocation()
	{
		return startLocation;
	}
	
	protected boolean movePlayer(Player player, int direction)
	{
		Location currLoc = player.getCurrentLocation();
		Location destination = currLoc.copy();
		int borderToCheck;

		int maxPosition = ((sectionDim * levelDim) - 1);
		
		// Check Level Boundaries and adjust destination if allowed
		if(direction == Location.LEFT)
		{
			if(currLoc.xCoordinate == 0)
			{
				return false;
			}
			else
			{
				destination.xCoordinate -= 1;
				borderToCheck = BasicOptions.EAST;
			}
		}
		else if(direction == Location.RIGHT)
		{
			if(currLoc.xCoordinate == maxPosition)
			{
				return false;
			}
			else
			{
				destination.xCoordinate += 1;
				borderToCheck = BasicOptions.WEST;
			}
		}
		else if(direction == Location.UP)
		{
			if(currLoc.yCoordinate == 0)
			{
				return false;
			}
			else
			{
				destination.yCoordinate -= 1;
				borderToCheck = BasicOptions.SOUTH;
			}
			
		}
		else if(direction == Location.DOWN)
		{
			if(currLoc.yCoordinate == maxPosition)
			{
				return false;
			}
			else
			{
				destination.yCoordinate += 1;
				borderToCheck = BasicOptions.NORTH;
			}
		}
		else
		{
			return false;
		}
		
		int borderFlag = getTileAt(destination.xCoordinate, destination.yCoordinate).getParameters()[borderToCheck];
		
		if(borderFlag == 1)
		{
			return false;
		}
		else
		{
			player.moveTo(destination);
			return true;
		}
	}
	
	protected boolean moveBlock(Location block, int direction)
	{
		Location destination = block.copy();
		int borderToCheck;

		int maxPosition = ((sectionDim * levelDim) - 1);
		
		// Check Level Boundaries and adjust destination if allowed
		if(direction == Location.LEFT)
		{
			if(block.xCoordinate == 0)
			{
				return false;
			}
			else
			{
				destination.xCoordinate -= 1;
				borderToCheck = BasicOptions.EAST;
			}
		}
		else if(direction == Location.RIGHT)
		{
			if(block.xCoordinate == maxPosition)
			{
				return false;
			}
			else
			{
				destination.xCoordinate += 1;
				borderToCheck = BasicOptions.WEST;
			}
		}
		else if(direction == Location.UP)
		{
			if(block.yCoordinate == 0)
			{
				return false;
			}
			else
			{
				destination.yCoordinate -= 1;
				borderToCheck = BasicOptions.SOUTH;
			}
			
		}
		else if(direction == Location.DOWN)
		{
			if(block.yCoordinate == maxPosition)
			{
				return false;
			}
			else
			{
				destination.yCoordinate += 1;
				borderToCheck = BasicOptions.NORTH;
			}
		}
		else
		{
			return false;
		}
		
		int borderFlag = getTileAt(destination.xCoordinate, destination.yCoordinate).getParameters()[borderToCheck];
		
		if(borderFlag == 1)
		{
			return false;
		}
		else
		{
			block.xCoordinate = destination.xCoordinate;
			block.yCoordinate = destination.yCoordinate;
			return true;
		}
	}
	
	protected void triggerTile(int type, int levelCol, int levelRow)
	{
		getTileAt(levelCol, levelRow).trigger(type);
	}
	
	protected char[][] getDrawData()
	{
		int totalDim = sectionDim * levelDim;
		char displayArray[][] = new char[totalDim][totalDim];
		
		// Split tiles and create panels
		for(int rIndex = 0; rIndex < levelDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < levelDim; cIndex++)
			{
				// Get Panels Array
				char panelArray[][] = panels[cIndex][rIndex].getDrawData();
						
				for(int tileRIndex = 0; tileRIndex < sectionDim; tileRIndex++)
				{
					for(int tileCIndex = 0; tileCIndex < sectionDim; tileCIndex++)
					{
						int mainArrayCol = (sectionDim * cIndex) + tileCIndex;
						int mainArrayRow = (sectionDim * rIndex) + tileRIndex;
								
						displayArray[mainArrayCol][mainArrayRow] = panelArray[tileCIndex][tileRIndex];
					}
				}
			}
		}
		
		return displayArray;
	}
	
	protected GamePanel[][] getPanels()
	{
		return panels;
	}
	
	protected GamePanel getPanelAt(int col, int row)
	{
		if(((col < 0) || (col >= levelDim)) || ((row < 0) || (row >= levelDim)))
		{
			return null;
		}
		else
		{
			return panels[col][row];
		}
	}
	
	protected void rotate(boolean clockwise)
	{
		transposeArray();
		
		if(clockwise)
		{
			reverseColumns();
		}
		else
		{
			reverseRows();
		}
	}
	
	protected GameTile getTileAt(int levelCol, int levelRow)
	{
		int destPanelCol = (levelCol / sectionDim);
		int destPanelRow = (levelRow / sectionDim);
		
		int destTileCol = (levelCol - (destPanelCol * sectionDim));
		int destTileRow = (levelRow - (destPanelRow * sectionDim));
		
		return panels[destPanelCol][destPanelRow].getTileAt(destTileCol, destTileRow);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void transposeArray()
	{
		GamePanel transpose[][] = new GamePanel[levelDim][levelDim];
		
		for(int rIndex = 0; rIndex < levelDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < levelDim; cIndex++)
			{
				transpose[rIndex][cIndex] = panels[cIndex][rIndex];
			}
		}
		
		panels = transpose;
	}
	
	private void reverseColumns()
	{
		GamePanel reverse[][] = new GamePanel[levelDim][levelDim];
		
		for(int rIndex = 0; rIndex < levelDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < levelDim; cIndex++)
			{
				reverse[(levelDim - cIndex - 1)][rIndex] = panels[cIndex][rIndex];
			}
		}
		
		panels = reverse;
	}
	
	private void reverseRows()
	{
		GamePanel reverse[][] = new GamePanel[levelDim][levelDim];
		
		for(int rIndex = 0; rIndex < levelDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < levelDim; cIndex++)
			{
				reverse[cIndex][(levelDim - rIndex - 1)] = panels[cIndex][rIndex];
			}
		}
		
		panels = reverse;
	}
}
