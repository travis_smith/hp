package tester.models.level;

import java.awt.Point;
import java.util.ArrayList;

import levelData.Tile;
import levelData.Trigger;
import fileIO.LevelKit;
import gui.tilepops.BasicOptions;
import gui.tilepops.StairsOptions;
import gui.tilepops.TeleportOptions;
import tester.models.Location;
import tester.models.Player;
import tester.models.level.specialTiles.Teleporter;

/**
The GameLevel will maintain the state of the currently loaded level. The class will
use LayerModels to hold the tile and trigger models. The main purpose of the 
GameLevel is to allow the engine and controller to modify data without needed to 
directly interact with layers or triggers. Instead, the engine will call methods
from the interface and these will return values based on what can or has been
accomplished. The GameLevel will also allow the view to retrieve needed rendering
data.

@author 	Travis Smith
@version 	1.0 | 14 May 2014
*/
public class GameLevel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size Data
	private int sectionDim;
	private int levelDim;
	
	// Layers
	private ArrayList<GameLayer> layers;
	private int activeLayer;
	
	// Trigger Manager
	private ArrayList<GameTrigger> triggers;
	
	// Display
	private boolean displayCurrent;
	
	// Special Purpose Arrays
	private ArrayList<Teleporter> teleporters;
	private ArrayList<Location> blocks;
	
	// Keys
	private int totalKeys;
	private int keysCollected;
	private boolean exitOpen;
	private boolean exitReached;
	
	// Player
	private Player player;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public GameLevel(LevelKit kit)
	{
		this.sectionDim = kit.getLevelData().getSectionDimension();
		this.levelDim = kit.getLevelData().getLevelDimension();
		this.totalKeys = 0;
		this.keysCollected = 0;
		this.exitReached = false;
		
		teleporters = new ArrayList<Teleporter>(0);
		blocks = new ArrayList<Location>(0);
		
		// Add Layers
		int numLayers = kit.getLevelData().getNumLayers();
		layers = new ArrayList<GameLayer>(numLayers);
		
		Tile[][][] tileData = kit.getTileData();
		
		for(int lIndex = 0; lIndex < numLayers; lIndex++)
		{
			// Get GameTile array
			int totalDim = (sectionDim * levelDim);
			GameTile layerTiles[][] = new GameTile[totalDim][totalDim];
			
			for(int rIndex = 0; rIndex < totalDim; rIndex++)
			{
				for(int cIndex = 0; cIndex < totalDim; cIndex++)
				{
					Tile tile = tileData[lIndex][cIndex][rIndex];
					
					if(tile.getType() == Tile.KEY_TILE)
					{
						totalKeys++;
					}
					else if(tile.getType() == Tile.TELE_TILE)
					{
						int teleID = tile.getParameters()[TeleportOptions.NAME];
						Location teleLoc = new Location(lIndex, cIndex, rIndex);
						
						Teleporter newTele = new Teleporter(teleID, teleLoc);
						
						teleporters.add(newTele);
					}
					else if(tile.getType() == Tile.BLOCK_TILE)
					{
						blocks.add(new Location(lIndex, cIndex, rIndex));
						layerTiles[cIndex][rIndex] = new GameTile(Tile.FLOOR_TILE, tile.getNumParameters(), tile.getParameters());
						continue;
					}
					
					layerTiles[cIndex][rIndex] = new GameTile(tile.getType(), tile.getNumParameters(), tile.getParameters());
				}
			}
			
			layers.add(new GameLayer(sectionDim, levelDim, layerTiles));
		}
		
		if(totalKeys == 0)
		{
			exitOpen = true;
		}
		else
		{
			exitOpen = false;
		}
		
		activeLayer = getStartLocation().layer;
		player = new Player(getStartLocation());
		
		// Add Triggers
		int numTriggers = kit.getTriggers().size();
		triggers = new ArrayList<GameTrigger>(numTriggers);
		
		for(int tIndex = 0; tIndex < numTriggers; tIndex++)
		{		
			triggers.add(new GameTrigger(kit.getTriggers().get(tIndex)));
		}
		
		displayCurrent = false;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void movePlayer(int direction)
	{
		Location origin = player.getCurrentLocation();
		
		
		if(layers.get(origin.layer).movePlayer(player, direction))
		{
			for(int index = 0; index < blocks.size(); index++)
			{
				if(player.getCurrentLocation().equals(blocks.get(index)))
				{
					if(layers.get(origin.layer).moveBlock(blocks.get(index), direction))
					{
						blockMoved(blocks.get(index), direction);
					}
					else
					{
						// Player cannot move, Block is in way and cannot move
						player.moveTo(origin);
						return;
					}
				}
			}
			
			playerMoved(origin, player.getCurrentLocation(), direction);
		}
	}
	
	public int getTotalDimension()
	{
		return (levelDim * sectionDim);
	}
	
	public char[][] getDrawData()
	{
		char displayArray[][] = layers.get(activeLayer).getDrawData();
		
		displayCurrent = true;
		
		return displayArray;
	}
	
	public ArrayList<Location> getBlockLocations()
	{
		ArrayList<Location> layerBlocks = new ArrayList<Location>(0);
		
		for(int index = 0; index < blocks.size(); index++)
		{
			if(blocks.get(index).layer == activeLayer)
			{
				layerBlocks.add(blocks.get(index).copy());
			}
		}
		
		return layerBlocks;
	}

	public void rotatePanels(boolean clockwise)
	{
		for(int index = 0; index < layers.size(); index++)
		{
			layers.get(index).rotate(clockwise);
		}

		// Rotate Player, blocks, triggers
		rotatePlayer(clockwise);
		rotateBlocks(clockwise);
		rotateTriggers(clockwise);
		rotateTeleporters(clockwise);
			
		displayCurrent = false;
	}
	
	public boolean isDisplayCurrent()
	{
		return displayCurrent;
	}
	
	public boolean exitReached()
	{
		return exitReached;
	}	
	
	public boolean playerDead() 
	{
		return player.isDead();
	}
	public Location getPlayerLocation()
	{
		return player.getCurrentLocation();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void rotatePlayer(boolean clockwise)
	{
		// Get panel coordinates, rotate them
		int panelPos[] = getPanelCoordinates(player.getCurrentLocation().xCoordinate, player.getCurrentLocation().yCoordinate);
		
		int newPos[] = new int[4];
		
		// Transpose Panel
		newPos[0] = panelPos[1];
		newPos[1] = panelPos[0];
		newPos[2] = panelPos[2];
		newPos[3] = panelPos[3];
		
		if(clockwise)
		{
			// Reverse Col
			newPos[0] = (levelDim - newPos[0] - 1);
		}
		else
		{
			// Reverse Row
			newPos[1] = (levelDim - newPos[1] - 1);
		}
		
		int lvlLoc[] = getLevelCoordinates(newPos);
		
		Location newLoc = new Location(player.getCurrentLocation().layer, lvlLoc[0], lvlLoc[1]);
		
		player.moveTo(newLoc);
	}
	
	private void rotateBlocks(boolean clockwise)
	{
		for(int index = 0; index < blocks.size(); index++)
		{
			// Get panel coordinates, rotate them
			int panelPos[] = getPanelCoordinates(blocks.get(index).xCoordinate, blocks.get(index).yCoordinate);
			
			int newPos[] = new int[4];
			
			// Transpose Panel
			newPos[0] = panelPos[1];
			newPos[1] = panelPos[0];
			newPos[2] = panelPos[2];
			newPos[3] = panelPos[3];
			
			if(clockwise)
			{
				// Reverse Col
				newPos[0] = (levelDim - newPos[0] - 1);
			}
			else
			{
				// Reverse Row
				newPos[1] = (levelDim - newPos[1] - 1);
			}
			
			int lvlLoc[] = getLevelCoordinates(newPos);
			
			blocks.get(index).xCoordinate = lvlLoc[0];
			blocks.get(index).yCoordinate = lvlLoc[1];
		}
	}
	
	private void rotateTeleporters(boolean clockwise)
	{
		for(int index = 0; index < teleporters.size(); index++)
		{
			// Get panel coordinates, rotate them
			int panelPos[] = getPanelCoordinates(teleporters.get(index).getLocation().xCoordinate, teleporters.get(index).getLocation().yCoordinate);
			
			int newPos[] = new int[4];
			
			// Transpose Panel
			newPos[0] = panelPos[1];
			newPos[1] = panelPos[0];
			newPos[2] = panelPos[2];
			newPos[3] = panelPos[3];
			
			if(clockwise)
			{
				// Reverse Col
				newPos[0] = (levelDim - newPos[0] - 1);
			}
			else
			{
				// Reverse Row
				newPos[1] = (levelDim - newPos[1] - 1);
			}
			
			int lvlLoc[] = getLevelCoordinates(newPos);
			
			teleporters.get(index).getLocation().xCoordinate = lvlLoc[0];
			teleporters.get(index).getLocation().yCoordinate = lvlLoc[1];
		}
	}
	
	private void rotateTriggers(boolean clockwise)
	{
		for(int index = 0; index < triggers.size(); index++)
		{
			// Get panel coordinates, rotate them
			int panelPos[] = getPanelCoordinates(triggers.get(index).getLocation().xCoordinate, triggers.get(index).getLocation().yCoordinate);
			
			int newPos[] = new int[4];
			
			// Transpose Panel
			newPos[0] = panelPos[1];
			newPos[1] = panelPos[0];
			newPos[2] = panelPos[2];
			newPos[3] = panelPos[3];
			
			if(clockwise)
			{
				// Reverse Col
				newPos[0] = (levelDim - newPos[0] - 1);
			}
			else
			{
				// Reverse Row
				newPos[1] = (levelDim - newPos[1] - 1);
			}
			
			int lvlLoc[] = getLevelCoordinates(newPos);
			
			triggers.get(index).getLocation().xCoordinate = lvlLoc[0];
			triggers.get(index).getLocation().yCoordinate = lvlLoc[1];
			
			triggers.get(index).rotateTargets(clockwise, levelDim, sectionDim);
		}
	}
	
	private int[] getPanelCoordinates(int levelCol, int levelRow)
	{
		int panelPosition[]= new int[4];
		
		panelPosition[0] = (levelCol / sectionDim);
		panelPosition[1] = (levelRow / sectionDim);
		
		panelPosition[2] = (levelCol - (panelPosition[0] * sectionDim));
		panelPosition[3] = (levelRow - (panelPosition[1] * sectionDim));
		
		return panelPosition;
	}
	
	private int[] getLevelCoordinates(int panelCoordinates[])
	{
		int levelCoord[] = new int[2];
		
		levelCoord[0] = (panelCoordinates[2] + (panelCoordinates[0] * sectionDim));
		levelCoord[1] = (panelCoordinates[3] + (panelCoordinates[1] * sectionDim));
		
		return levelCoord;
	}
	
	private Location getStartLocation()
	{
		Location start;
		
		for(int lIndex = 0; lIndex < layers.size(); lIndex++)
		{
			if(layers.get(lIndex).hasStart())
			{
				Point gridLoc = layers.get(lIndex).getStartLocation();
				
				start = new Location(lIndex, gridLoc.x, gridLoc.y);
				
				return start;
			}
		}
		
		start = new Location();
		return start;
	}
	
	private void blockMoved(Location block, int direction)
	{
		GameTrigger blockTrigger = null;
		
		// Check Triggers
		for(int tIndex = 0; tIndex < triggers.size(); tIndex++)
		{
			if((triggers.get(tIndex).getLocation().equals(block)) && 
				(triggers.get(tIndex).getTriggerMode() == Trigger.BLOCK_ACTIVATED))
			{
				blockTrigger = triggers.get(tIndex);
				break;
			}
		}
		
		// Fire Block Trigger
		if(blockTrigger != null)
		{
			int type = blockTrigger.getType();
			ArrayList<Location> targets = blockTrigger.getTargetArray();
			
			for(int index = 0; index < targets.size(); index++)
			{
				layers.get(targets.get(index).layer).triggerTile(type, targets.get(index).xCoordinate,
																	   targets.get(index).yCoordinate);
			}
			
			displayCurrent = false;
			
			blocks.remove(block);
		}
		
		// Handle special tile behavior
		GameTile enteredTile = layers.get(block.layer).getTileAt(block.xCoordinate, block.yCoordinate);
		
		if(enteredTile.getParameters()[BasicOptions.ACTIVATION] == 1)
		{

			if(enteredTile.getType() == Tile.TELE_TILE)
			{
				// Implement if wanted
			}
			else if(enteredTile.getType() == Tile.ICE_TILE)
			{
				if(layers.get(block.layer).moveBlock(block, direction))
				{
					blockMoved(block, direction);
				}
			}
		}
	}
	
	private void playerMoved(Location oldLoc, Location newLoc, int direction)
	{
		GameTrigger exitTrig = null;
		GameTrigger enterTrig = null;
		
		// Check Triggers
		for(int tIndex = 0; tIndex < triggers.size(); tIndex++)
		{
			if((triggers.get(tIndex).getLocation().equals(newLoc)) && 
				(triggers.get(tIndex).getTriggerMode() == Trigger.ON_ENTER))
			{
				enterTrig = triggers.get(tIndex);
			}
			else if((triggers.get(tIndex).getLocation().equals(oldLoc)) && 
					(triggers.get(tIndex).getTriggerMode() == Trigger.ON_EXIT))
			{
				exitTrig = triggers.get(tIndex);
			}
		}

		// Fire Exit Trigger
		if(exitTrig != null)
		{
			int type = exitTrig.getType();
			ArrayList<Location> targets = exitTrig.getTargetArray();
			
			for(int index = 0; index < targets.size(); index++)
			{
				layers.get(targets.get(index).layer).triggerTile(type, targets.get(index).xCoordinate,
																	   targets.get(index).yCoordinate);
			}
			
			displayCurrent = false;
		}
		
		// Fire Enter Trigger
		if(enterTrig != null)
		{
			int type = enterTrig.getType();
			ArrayList<Location> targets = enterTrig.getTargetArray();
			
			for(int index = 0; index < targets.size(); index++)
			{
				layers.get(targets.get(index).layer).triggerTile(type, targets.get(index).xCoordinate,
																	   targets.get(index).yCoordinate);
			}
			
			displayCurrent = false;
		}
		
		// Handle special tile behavior
		GameTile enteredTile = layers.get(newLoc.layer).getTileAt(newLoc.xCoordinate, newLoc.yCoordinate);
		
		if(enteredTile.getParameters()[BasicOptions.ACTIVATION] == 1)
		{
			if(enteredTile.getType() == Tile.KEY_TILE)
			{
				enteredTile.trigger(Trigger.DEACTIVATING);
				keysCollected++;
				displayCurrent = false;
				
				if(keysCollected == totalKeys)
				{
					exitOpen = true;
				}
			}
			else if(enteredTile.getType() == Tile.EXIT_TILE)
			{
				if(exitOpen)
				{
					exitReached = true;
				}
			}
			else if(enteredTile.getType() == Tile.STAIRS_TILE)
			{
				int destLayer = enteredTile.getParameters()[StairsOptions.DESTINATION];
				activeLayer = destLayer;
				player.moveTo(new Location(destLayer, newLoc.xCoordinate, newLoc.yCoordinate));	
				
				displayCurrent = false;
			}
			else if(enteredTile.getType() == Tile.TELE_TILE)
			{
				int destID = enteredTile.getParameters()[TeleportOptions.DESTINATION];
				Location dest;
				
				for(int index = 0; index < teleporters.size(); index++)
				{
					if(destID == teleporters.get(index).getID())
					{
						dest = teleporters.get(index).getLocation();
						player.moveTo(dest);
						
						if(dest.layer != activeLayer)
						{
							activeLayer = dest.layer;
							displayCurrent = false;
						}
						
						break;
					}
				}
				
			}
			else if(enteredTile.getType() == Tile.ICE_TILE)
			{
				movePlayer(direction);
			}
			else if(enteredTile.getType() == Tile.SPIKE_TILE)
			{
				player.kill();
			}
		}
	}
}
