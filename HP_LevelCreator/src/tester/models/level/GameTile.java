package tester.models.level;

import gui.tilepops.BasicOptions;
import levelData.Trigger;

public class GameTile 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
		
	private char type;
	private int numParameters;
	private int parameters[];

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected GameTile(char type, int numParameters, int parameters[])
	{
		this.type = type;
		this.numParameters = numParameters;
		this.parameters = parameters;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void playerEntered()
	{
		// TODO
	}
	
	protected void trigger(int type)
	{
		if(type == Trigger.ACTIVATING)
		{
			parameters[BasicOptions.ACTIVATION] = 1;
		}
		else if(type == Trigger.DEACTIVATING)
		{
			parameters[BasicOptions.ACTIVATION] = 0;
		}
		else if(type == Trigger.TOGGLE)
		{
			if(parameters[BasicOptions.ACTIVATION] == 1)
			{
				parameters[BasicOptions.ACTIVATION] = 0;
			}
			else
			{
				parameters[BasicOptions.ACTIVATION] = 1;
			}
		}
	}
	
	protected char getType()
	{
		return type;
	}
	
	protected int getNumParameters()
	{
		return numParameters;
	}
	
	protected int[] getParameters()
	{
		return parameters;
	}
}
