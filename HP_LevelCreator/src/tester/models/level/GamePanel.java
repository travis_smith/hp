package tester.models.level;

import levelData.Tile;
import gui.tilepops.BasicOptions;

/**
The GamePanel will hold an array of tiles that combine to form one section of the level.

@author 	Travis Smith
@version 	1.0 | 14 May 2014
*/
public class GamePanel 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Size
	private int sectionDim;
	
	// Panels
	private GameTile tileGrid[][];
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	protected GamePanel(int sectionDim, GameTile tileGrid[][])
	{
		this.sectionDim = sectionDim;
		this.tileGrid = tileGrid;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected char[][] getDrawData()
	{
		char displayArray[][] = new char[sectionDim][sectionDim];
		
		for(int rIndex = 0; rIndex < sectionDim; rIndex++)
		{
			for(int cIndex = 0; cIndex < sectionDim; cIndex++)
			{
				if(tileGrid[cIndex][rIndex].getParameters()[BasicOptions.ACTIVATION] == 1)
				{
					displayArray[cIndex][rIndex] = tileGrid[cIndex][rIndex].getType();
				}
				else if(tileGrid[cIndex][rIndex].getType() == Tile.WALL_TILE)
				{
					displayArray[cIndex][rIndex] = Tile.WALL_TILE;
				}
				else
				{
					displayArray[cIndex][rIndex] = Tile.FLOOR_TILE;
				}
			}
		}
		
		return displayArray;
	}
	
	protected GameTile[][] getTileGrid()
	{
		return tileGrid;
	}
	
	protected GameTile getTileAt(int col, int row)
	{
		if(((col < 0) || (col >= sectionDim)) || ((row < 0) || (row >= sectionDim)))
		{
			return null;
		}
		else
		{
			return tileGrid[col][row];
		}
	}
	
	protected void rotate(boolean clockwise)
	{
		// TODO
		// This method will rotate the tilegrid array within the panel in the direction
		// Specified by the passed boolean
	}
}
