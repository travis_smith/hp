package tester.models.level.specialTiles;

import tester.models.Location;

public class Teleporter 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	private int id;
	private Location location;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Teleporter(int id, Location location)
	{
		this.id = id;
		this.location = location;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public int getID()
	{
		return id;
	}
	
	public Location getLocation()
	{
		return location;
	}
}
