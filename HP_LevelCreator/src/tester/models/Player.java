package tester.models;

/**
The Player model will hold information regarding the current and previous locations
of the player. It will contain a public interface that allows the player to be moved.

@author 	Travis Smith
@version 	1.0 | 14 May 2014
*/
public class Player 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Location Members
	private Location currentLoc;
	private boolean isDead;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Player()
	{
		currentLoc = new Location();
		isDead = false;
	}
	
	public Player(Location start)
	{
		currentLoc = start;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void moveTo(Location newLocation)
	{
		currentLoc = newLocation.copy();
	}
	
	public Location getCurrentLocation()
	{
		return currentLoc.copy();
	}
	
	public void kill()
	{
		isDead = true;
	}
	
	public boolean isDead()
	{
		return isDead;
	}
}
