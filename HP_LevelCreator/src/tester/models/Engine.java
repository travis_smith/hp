package tester.models;

import fileIO.LevelKit;
import start.EditorMain;
import tester.controller.InputHandler;
import tester.models.level.GameLevel;
import tester.view.TesterScreen;

public class Engine implements Runnable
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Engine Data
	private boolean running;
	
	// Models
	private GameLevel level;
	private LevelKit currentKit;
	
	// View
	private final TesterScreen screenRfc;
	
	// Control
	private final InputHandler controller;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public Engine(LevelKit kit, final TesterScreen screen, final InputHandler controller)
	{
		this.currentKit = kit;
		this.running = false;
		this.screenRfc = screen;
		this.controller = controller;
		
		level = new GameLevel(currentKit.copy());
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public synchronized void start() 
	{
		running = true;
		new Thread(this).start();
	}

	public synchronized void stop() 
	{
		running = false;
	}

	@Override
	public void run() 
	{
		while(running)
		{
			handleInput();
			
			// Check if level model has changed and update screen
			if(!level.isDisplayCurrent())
			{
				screenRfc.updateMainImage(level.getDrawData(), level.getTotalDimension());
			}
			
			if((level.exitReached()) || (level.playerDead()))
			{
				reset();
			}
			
			// Draw Screen
			screenRfc.render(level.getPlayerLocation(), level.getBlockLocations());
		}
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void handleInput()
	{
		int input = controller.getLastInput();
		
		if(input == InputHandler.NONE)
		{
			return;
		}
		else if(input == InputHandler.ESC_KEY)
		{
			quit();
		}
		else if(input == InputHandler.ENTER_KEY)
		{
			reset();
		}
		else if(input == InputHandler.W_KEY)
		{
			movePlayer(Location.UP);
		}
		else if(input == InputHandler.A_KEY)
		{
			movePlayer(Location.LEFT);
		}
		else if(input == InputHandler.S_KEY)
		{
			movePlayer(Location.DOWN);
		}
		else if(input == InputHandler.D_KEY)
		{
			movePlayer(Location.RIGHT);
		}
		else if(input == InputHandler.Z_KEY)
		{
			rotateMap(false);
		}
		else if(input == InputHandler.X_KEY)
		{
			rotateMap(true);
		}
	}
	
	private void movePlayer(int direction)
	{
		level.movePlayer(direction);
	}
	
	private void rotateMap(boolean clockwise)
	{
		level.rotatePanels(clockwise);
	}
	
	private void reset()
	{
		level = new GameLevel(currentKit.copy());
	}
	
	private void quit()
	{
		stop();
		EditorMain.getInstance().testerClosed();
	}
}
