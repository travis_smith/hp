package tester.models;

/**
The Location class is used to encapsulate the three integers that represent an objects
location on the level. These three integers represent the current layer, X-coordinate
and Y-coordinate.

The location class is intended to be used privately within a class in order to protect
the data; however, the class data members themselves will be public for ease of access.

@author 	Travis Smith
@version 	1.0 | 14 May 2014
*/

public class Location 
{
	// ============================================================================================
	// Data Members
	// ============================================================================================	
	
	// Direction Constants
	public static final int UP = 0;
	public static final int RIGHT = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	
	public int layer;
	public int xCoordinate;
	public int yCoordinate;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	public Location()
	{
		this.layer = 0;
		this.xCoordinate = 0;
		this.yCoordinate = 0;
	}
	
	public Location(int layer, int xCoordinate, int yCoordinate)
	{
		this.layer = layer;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public boolean equals(Location other)
	{
		if((this.layer == other.layer) && 
		   (this.xCoordinate == other.xCoordinate) && 
		   (this.yCoordinate == other.yCoordinate))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Location copy()
	{
		Location copy = new Location(layer, xCoordinate, yCoordinate);
		
		return copy;
	}
}
