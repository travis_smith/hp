package tester.view;

import java.awt.GraphicsEnvironment;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import start.EditorMain;
import tester.controller.InputHandler;
import tester.models.Engine;
import fileIO.LevelKit;

@SuppressWarnings("serial")
public class TesterFrame extends JInternalFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Models
	private Engine engine;
	
	// View
	private TesterScreen screen;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public TesterFrame(LevelKit kit)
	{
		super(kit.getLevelData().getLevelName(), false);
		
		screen = new TesterScreen();
		JPanel mainContent = new JPanel();
		InputHandler controller = new InputHandler();
		controller.createInputMap(mainContent);
		this.setContentPane(mainContent);
		engine = new Engine(kit, screen, controller);
		
		// Set-Up Frame
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addInternalFrameListener(new InternalFrameAdapter()
		{
			@Override
			public void internalFrameClosing(InternalFrameEvent arg0) 
			{
				engine.stop();
				EditorMain.getInstance().testerClosed();
			}
		});
		
		this.setClosable(true);
		this.getContentPane().add(screen);
		this.pack();
		this.setLocation(getScreenCenter());
		this.setVisible(false);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public void initScreen()
	{
		this.screen.initializeBufferStrategy();
	}
	
	public void start()
	{
		this.engine.start();
	}
	
	public void stop()
	{
		this.engine.stop();
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private Point getScreenCenter()
	{
		GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point center = gEnv.getCenterPoint();
		
		center.x -= (this.getWidth() / 2);
		center.y -= (this.getHeight() / 2);
		
		return center;
	}
}
