package fileIO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import levelData.Level;
import levelData.Tile;
import levelData.Trigger;
import levelData.Trigger.Target;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class FileIO 
{
	public static void saveLevel(LevelKit kit)
	{
		Level level = kit.getLevelData();
		Tile[][][] tiles = kit.getTileData();
		
		int numLayers = level.getNumLayers();
		int lvlDim = level.getLevelDimension();
		int secDim = level.getSectionDimension();

		// Create Root Element and Document
		Element rootElem = new Element("level");
		Document levelDoc = new Document(rootElem);
		
		// Add Basic Level Data
		levelDoc.getRootElement().addContent(new Element("version").setText(Integer.toString(LevelKit.CURRENT_VERSION)));
		levelDoc.getRootElement().addContent(new Element("name").setText(level.getLevelName()));
		levelDoc.getRootElement().addContent(new Element("r-type").setText(Integer.toString( level.getRotationType())));
		levelDoc.getRootElement().addContent(new Element("numLayers").setText(Integer.toString(numLayers)));
		levelDoc.getRootElement().addContent(new Element("lvlDim").setText(Integer.toString(lvlDim)));
		levelDoc.getRootElement().addContent(new Element("secDim").setText(Integer.toString(secDim)));
		levelDoc.getRootElement().addContent(new Element("teleporters").setText(Integer.toString(level.getNumTeleporters())));
		levelDoc.getRootElement().addContent(new Element("nextTrigger").setText(Integer.toString(level.getNextTriggerID())));
		
		// Add Layers
		int gridDimension = (lvlDim * secDim);
		for(int layerIndex = 0; layerIndex < numLayers; layerIndex++)
		{
			Element layerElem = new Element("layer");
				
			// Add Tiles
			Tile layerGrid[][] = tiles[layerIndex];
				
			for(int tileRIndex = 0; tileRIndex < gridDimension; tileRIndex++)
			{
				for(int tileCIndex = 0; tileCIndex < gridDimension; tileCIndex++)
				{
					Element tileElem = new Element("tile");
						
					char type = layerGrid[tileCIndex][tileRIndex].getType();
					tileElem.addContent(new Element("type").setText(Character.toString(type)));
						
					int numParam = layerGrid[tileCIndex][tileRIndex].getNumParameters();
					tileElem.addContent(new Element("numParams").setText(Integer.toString(numParam)));
						
					int params[] = layerGrid[tileCIndex][tileRIndex].getParameters();
						
					// Add Params
					for(int pIndex = 0; pIndex < numParam; pIndex++)
					{
						tileElem.addContent(new Element("parameter").setText(Integer.toString(params[pIndex])));
					}

					layerElem.addContent(tileElem);
				}
			}
			
			levelDoc.getRootElement().addContent(layerElem);
		}
		
		// Add Triggers
		ArrayList<Trigger> triggers = kit.getTriggers();
		
		for(int triggerIndex = 0; triggerIndex < triggers.size(); triggerIndex++)
		{
			Element triggerElem = new Element("trigger");
			triggerElem.addContent(new Element("id").setText(Integer.toString(triggers.get(triggerIndex).getIdNumber())));
			triggerElem.addContent(new Element("type").setText(Integer.toString(triggers.get(triggerIndex).getType())));
			triggerElem.addContent(new Element("whenTriggers").setText(Integer.toString(triggers.get(triggerIndex).getWhenTriggers())));
			triggerElem.addContent(new Element("layer").setText(Integer.toString(triggers.get(triggerIndex).getLocation()[0])));
			triggerElem.addContent(new Element("x").setText(Integer.toString(triggers.get(triggerIndex).getLocation()[1])));
			triggerElem.addContent(new Element("y").setText(Integer.toString(triggers.get(triggerIndex).getLocation()[2])));
			
			ArrayList<Target> targets = triggers.get(triggerIndex).getTargets();
			
			for(int tarIndex = 0; tarIndex < targets.size(); tarIndex++)
			{
				Element targetElem = new Element("target");
				
				targetElem.addContent(new Element("layer").setText(Integer.toString(targets.get(tarIndex).getLocation()[0])));
				targetElem.addContent(new Element("x").setText(Integer.toString(targets.get(tarIndex).getLocation()[1])));
				targetElem.addContent(new Element("y").setText(Integer.toString(targets.get(tarIndex).getLocation()[2])));
				
				triggerElem.addContent(targetElem);
			}
			
			levelDoc.getRootElement().addContent(triggerElem);
		}
		
		// Save Document Out
		XMLOutputter xmlOut = new XMLOutputter();
		
		String filePath = new String("resources\\levels\\" + level.getLevelName() + ".xml");
		
		xmlOut.setFormat(Format.getPrettyFormat());
		
		try 
		{
			File xmlFile = new File(filePath);
			xmlOut.output(levelDoc, new FileWriter(xmlFile.getPath()));
			System.out.println("Saved");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			System.err.println("IO Exception");
		} 
	}
	
	public static LevelKit loadLevel(String levelName)
	{
		File xmlFile = new File("resources\\levels\\" + levelName + ".xml");
		
		return loadData(xmlFile);
	}

	public static LevelKit loadLevel()
	{
		File filePath = new File("resources\\levels\\");

		// Set-Up File Chooser Frame
		JFrame loadFrame = new JFrame();
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(filePath);
		int returnVal;
		File fileName;
		
		returnVal = fileChooser.showOpenDialog(loadFrame);
		
		// Check User Selection and Load File
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			fileName = fileChooser.getSelectedFile();
			loadFrame.dispose();
			
			return loadData(fileName);
		}	
		else if(returnVal == JFileChooser.CANCEL_OPTION)
		{
			System.out.println("Canceled");
			return null;
		}
		else
		{
			System.err.println("Error With File Chooser");
			return null;
		}
	}

	private static LevelKit loadData(File xmlFile)
	{
		// Open File for Loading		
		SAXBuilder builder = new SAXBuilder();
		
		try
		{
			Document document = (Document)builder.build(xmlFile);
			Element rootNode = document.getRootElement();	
		
			Element workingNode;
			
			// Level Data
			String name;
			int rType;
			int numLayers;
			int lvlDim;
			int secDim;
			int numTeleporters;
			int nextTrigger;
			
			// Check Version
			workingNode = rootNode.getChild("version");
			if(workingNode == null)
			{
				System.out.println("No Version Recorded");
				return null;
			}
			else
			{
				int version = Integer.parseInt(workingNode.getText());
				
				if(version != LevelKit.CURRENT_VERSION)
				{
					System.out.println("Incorrect Version Detected");
					return null;					
				}
			}
			
			// Get Name
			workingNode = rootNode.getChild("name");
			name = workingNode.getText();
			
			// Get R-Type
			workingNode = rootNode.getChild("r-type");		
			rType = Integer.parseInt(workingNode.getText());
			
			// Get Num Layers
			workingNode = rootNode.getChild("numLayers");
			numLayers = Integer.parseInt(workingNode.getText());
			
			// Get Level Dim
			workingNode = rootNode.getChild("lvlDim");		
			lvlDim = Integer.parseInt(workingNode.getText());
			
			// Get Section Dim
			workingNode = rootNode.getChild("secDim");		
			secDim = Integer.parseInt(workingNode.getText());
			
			// Get Num Teleporters
			workingNode = rootNode.getChild("teleporters");
			numTeleporters = Integer.parseInt(workingNode.getText());
			
			// Get Next Trigger ID
			workingNode = rootNode.getChild("nextTrigger");
			nextTrigger = Integer.parseInt(workingNode.getText());
			
			// Initialize Level Data
			Level levelData = new Level(name, numLayers, lvlDim, secDim, rType, numTeleporters, nextTrigger);

			// Build Tile Data
			int gridDimension = (lvlDim * secDim);
			Tile tileData[][][] = new Tile[numLayers][gridDimension][gridDimension];
			List<Element> layerList = rootNode.getChildren("layer");
			
			Element currentLayerNode;
			Element currentTileNode;
			
			int tileCIndex = 0;
			int tileRIndex = 0;
			
			for(int lIndex = 0; lIndex < layerList.size(); lIndex++)
			{
				// Get Section Node
				currentLayerNode = layerList.get(lIndex);

				// Allocate space for TileData Array
				Tile tiles[][] = new Tile[gridDimension][gridDimension];
				List<Element> tileList = currentLayerNode.getChildren("tile");
				
				tileCIndex = 0;
				tileRIndex = 0;
						
				for(int tIndex = 0; tIndex < tileList.size(); tIndex++)
				{
					currentTileNode = tileList.get(tIndex);
					
					char type;
					int numParams;
					int params[];
					
					// Get Tile Type
					workingNode = currentTileNode.getChild("type");		
					type = (workingNode.getText()).charAt(0);
					
					// Get Number of Parameters
					workingNode = currentTileNode.getChild("numParams");		
					numParams = Integer.parseInt(workingNode.getText());
					
					// Get Parameters
					params = new int[numParams];
					List<Element> paramList = currentTileNode.getChildren("parameter");
					
					for(int pIndex = 0; pIndex < numParams; pIndex++)
					{
						// Get Parameter
						workingNode = paramList.get(pIndex);		
						int temp = Integer.parseInt(workingNode.getText());
						
						params[pIndex] = temp;
					}
					
					tiles[tileCIndex][tileRIndex] = new Tile(type, numParams, params);
					
					// Update Tile Indexes
					tileCIndex++;
					
					if(tileCIndex >= gridDimension)
					{
						tileCIndex = 0;
						tileRIndex++;
					}
				}
						
				// Add tileGrid to data array
				tileData[lIndex] = tiles;				
			}
			
			// Get Triggers
			List<Element> triggerList = rootNode.getChildren("trigger");		
			
			Element currentTriggerNode;
			Element currentTargetNode;
			
			ArrayList<Trigger> loadedTriggers = new ArrayList<Trigger>(triggerList.size());
			
			for(int trigIndex = 0; trigIndex < triggerList.size(); trigIndex++)
			{
				currentTriggerNode = triggerList.get(trigIndex);
				
				// Get Location
				int trigID = Integer.parseInt(currentTriggerNode.getChildText("id"));
				int trigType = Integer.parseInt(currentTriggerNode.getChildText("type"));
				int trigWhen = Integer.parseInt(currentTriggerNode.getChildText("whenTriggers"));
				int trigLayer = Integer.parseInt(currentTriggerNode.getChildText("layer"));
				int trigX = Integer.parseInt(currentTriggerNode.getChildText("x"));
				int trigY = Integer.parseInt(currentTriggerNode.getChildText("y"));
				
				Trigger newTrigger = new Trigger(trigID, trigType, trigWhen, trigLayer, trigX, trigY);
				
				// Get Targets
				List<Element> targetList = currentTriggerNode.getChildren("target");
				
				for(int tarIndex = 0; tarIndex < targetList.size(); tarIndex++)
				{
					currentTargetNode = targetList.get(tarIndex);
				
					int tarLayer = Integer.parseInt(currentTargetNode.getChildText("layer"));
					int tarX = Integer.parseInt(currentTargetNode.getChildText("x"));
					int tarY = Integer.parseInt(currentTargetNode.getChildText("y"));
					
					int targetLoc[] = new int[]{tarLayer, tarX, tarY}; 
					
					newTrigger.addTarget(targetLoc);
				}
				
				loadedTriggers.add(newTrigger);
			}
			
			// Initialize kit
			LevelKit loadedKit = new LevelKit(levelData, tileData, loadedTriggers);
			
			return loadedKit;
		}	
		catch (JDOMException e) 
		{
			e.printStackTrace();
			System.err.println("JDOM Exception");
			return null;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			System.err.println("IO Exception");
			return null;
		}
	}
}
