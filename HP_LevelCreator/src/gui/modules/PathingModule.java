package gui.modules;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PathingModule extends EditorModule
{
	// ============================================================================================
	// Constructor
	// ============================================================================================
	public PathingModule()
	{
		super("Basic Layout");
		
		this.buildGUI();
		this.pack();
		this.setVisible(false);	
	}

	// ============================================================================================
	// Implemented Abstract Methods
	// ============================================================================================

	@Override
	protected void buildGUI() 
	{		
		JPanel mainContent = new JPanel();
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.Y_AXIS));
		mainContent.add(buildMessagePanel());
		mainContent.add(buildInstructionPanel());
		
		this.setContentPane(mainContent);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================

	private JPanel buildInstructionPanel()
	{
		String placeSingleText = "Left-Click to set Tiles as Corridors";
		String removeSingleText = "Right-Click to set Tiles as Walls";
		String shiftCommentOne = "Holding SHIFT while clicking will create an area";
		String shiftCommentTwo = "between the two clicked tiles. The area will be";
		String shiftCommentThree = "defined by the second click type.";
		
		JLabel plcSingleField = new JLabel(placeSingleText);
		JLabel rmvSingleField = new JLabel(removeSingleText);
		JLabel shiftFieldOne = new JLabel(shiftCommentOne);
		JLabel shiftFieldTwo = new JLabel(shiftCommentTwo);
		JLabel shiftFieldThree = new JLabel(shiftCommentThree);
		
		JPanel instructPanel = new JPanel();
		instructPanel.setLayout(new BoxLayout(instructPanel, BoxLayout.Y_AXIS));
		instructPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		instructPanel.setAlignmentX(CENTER_ALIGNMENT);

		instructPanel.add(Box.createVerticalStrut(3));
		instructPanel.add(plcSingleField);
		instructPanel.add(Box.createVerticalStrut(10));
		instructPanel.add(rmvSingleField);
		instructPanel.add(Box.createVerticalStrut(10));
		instructPanel.add(shiftFieldOne);
		instructPanel.add(shiftFieldTwo);
		instructPanel.add(shiftFieldThree);
		instructPanel.add(Box.createVerticalStrut(3));
		
		return instructPanel;
	}
}
