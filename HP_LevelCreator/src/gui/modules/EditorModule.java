package gui.modules;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Base Class for all Editing modules in the tool set.
 * @author Travis
 * @see javax.swing.JInternalFrame
 */
@SuppressWarnings("serial")
public abstract class EditorModule extends JInternalFrame
{
	// ============================================================================================
	// Shared Members
	// ============================================================================================
	
	// Core Data
	private String title;
	
	// GUI
	protected JTextField messageField;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	public EditorModule(String title)
	{
		super(title, false, false); 
		this.title = title;
	}
	// ============================================================================================
	// Public Members
	// ============================================================================================
	
	public String getTitle()
	{
		return title;
	}
	
	// ============================================================================================
	// Abstract Methods
	// ============================================================================================

	protected abstract void buildGUI();
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	protected JPanel buildMessagePanel()
	{
		JPanel msgPanel = new JPanel();
		msgPanel.setLayout(new BoxLayout(msgPanel, BoxLayout.Y_AXIS));
		msgPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
						 , BorderFactory.createMatteBorder(1, 0, 1, 0, Color.GRAY)));
		
		messageField = new JTextField("Messages");
		messageField.setMaximumSize(new Dimension(500, 50));
		messageField.setHorizontalAlignment(JTextField.CENTER);
		messageField.setEditable(false);
		
		msgPanel.add(messageField);
		return msgPanel;
	}
	
	protected void setMessage(String msg)
	{
		messageField.setText(msg);
	}
}
