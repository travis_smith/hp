package gui.modules;

// Imports
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import levelData.Tile;

/**
The TileModule will act as a "palette" of available tiles for the user to "paint" onto the
MapPanel's tiles. Essentially this panel allows the user to select which type of tile they want
to place and then place it on the tile grid inside the MapPanel.


@author 	Travis Smith
@version 	1.0 | 19 Mar 2014
*/
@SuppressWarnings("serial")
public class TileModule extends EditorModule
{
	// ============================================================================================
	// Data Members
	// ============================================================================================

	// Size Constants
	static private final int OPTION_COLUMNS = 4;
	static private final int OPTION_MARGIN = 5;
	static private final int PANEL_BUFFER = 10;
	
	// GUI Members
	private TileOption currentSelection;
	private TileOption options[];
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public TileModule()
	{
		super("Tiles");

		this.buildGUI();
		this.pack();
		this.setVisible(false);	
	}
	

	// ============================================================================================
	// Implemented Abstract Methods
	// ============================================================================================

	@Override
	protected void buildGUI() 
	{
		JPanel mainContent = new JPanel();
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.Y_AXIS));
		mainContent.add(buildMainPanel());
		
		this.setContentPane(mainContent);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	// Getters
	public char getSelectedType()
	{
		return currentSelection.getTileType();
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	/**
	 * Assigns the clicked SelectionOption's data to the currentSelection
	 * 
	 * @param option	-	the SelectionOption the user clicked
	 */
	protected void optionClicked(TileOption option)
	{
		currentSelection.setTileType(option.getTileType());
		currentSelection.setTileName(option.getTileName());
		currentSelection.setImageX(option.getImageX());
		currentSelection.setImageY(option.getImageY());
		currentSelection.repaint();
	}

	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	/**
	 * Will construct and arrange the various GUI elements associated with this class.
	 * 
	 * @return tilePanel - the JPanel used to hold all GUI elements for this class
	 */
	private JPanel buildMainPanel()
	{	
		buildSelectionOptions();

		// Set-Up Options Panel
		JPanel optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridLayout(0, OPTION_COLUMNS, OPTION_MARGIN, OPTION_MARGIN));
	
		for(int index = 0; index < Tile.NUM_TYPES; index++)
		{
			optionsPanel.add(options[index]);
		}

		// Build Preview panel
		currentSelection = new TileOption(this, Tile.WALL_TILE, "Wall", 0, 0);
		currentSelection.setEnabled(false);
		currentSelection.setToolTipText(null);
			
		JPanel previewPanel = new JPanel();
		previewPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20)
				 			 , BorderFactory.createMatteBorder(2, 0, 2, 0, Color.GRAY)));
		previewPanel.add(new JLabel("Current Tile: "));
		previewPanel.add(currentSelection);

		JPanel tilePanel = new JPanel();
		tilePanel.setBorder(BorderFactory.createEmptyBorder(PANEL_BUFFER, PANEL_BUFFER, PANEL_BUFFER, PANEL_BUFFER));
		tilePanel.setLayout(new BoxLayout(tilePanel, BoxLayout.Y_AXIS));
		tilePanel.add(previewPanel);
		tilePanel.add(optionsPanel);
		
		return tilePanel;
	}
	
	/**
	 * Will initialize the options array and a SelectionOption for each type of tile
	 * currently implemented.
	 * <p>
	 * This is a horrible method and needs to be rebuilt when time permits.
	 */
	private void buildSelectionOptions()
	{
		Point imageLoc;
		options = new TileOption[Tile.NUM_TYPES];
		
		// Wall Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.WALL_TILE);
		options[0] = new TileOption(this, Tile.WALL_TILE, "Wall", imageLoc.x, imageLoc.y);
		
		// Start Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.START_TILE);
		options[1] = new TileOption(this, Tile.START_TILE, "Start", imageLoc.x, imageLoc.y);
		
		// Key Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.KEY_TILE);
		options[2] = new TileOption(this, Tile.KEY_TILE, "Key", imageLoc.x, imageLoc.y);
		
		// Exit Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.EXIT_TILE);
		options[3] = new TileOption(this, Tile.EXIT_TILE, "Exit", imageLoc.x, imageLoc.y);
		
		// Floor Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.FLOOR_TILE);
		options[4] = new TileOption(this, Tile.FLOOR_TILE, "Floor", imageLoc.x, imageLoc.y);
		
		// Ice Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.ICE_TILE);
		options[5] = new TileOption(this, Tile.ICE_TILE, "Ice", imageLoc.x, imageLoc.y);
		
		// Teleporter Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.TELE_TILE);
		options[6] = new TileOption(this, Tile.TELE_TILE, "Teleporter", imageLoc.x, imageLoc.y);
		
		// Stairs Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.STAIRS_TILE);
		options[7] = new TileOption(this, Tile.STAIRS_TILE, "Stairs", imageLoc.x, imageLoc.y);
		
		// Spikes Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.SPIKE_TILE);
		options[8] = new TileOption(this, Tile.SPIKE_TILE, "Spikes", imageLoc.x, imageLoc.y);
		
		// Block Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.BLOCK_TILE);
		options[9] = new TileOption(this, Tile.BLOCK_TILE, "Push Block", imageLoc.x, imageLoc.y);
		
		// Block Switch Option
		imageLoc = ImageControl.lookupImageCoordinates(Tile.BLOCK_SWITCH);
		options[10] = new TileOption(this, Tile.BLOCK_SWITCH, "Block Switch", imageLoc.x, imageLoc.y);
	}
}

