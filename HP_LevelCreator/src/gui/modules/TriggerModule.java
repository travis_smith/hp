package gui.modules;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import levelData.Trigger;
import start.EditorMain;

@SuppressWarnings("serial")
public class TriggerModule extends EditorModule
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// State Constants
	private final static int CREATING = 0;
	private final static int PLACING = 1;
	private final static int TARGETS = 2;
	
	// Data
	private int nextID;
	private int nextType;
	private ArrayList<Trigger> triggers;
	private int state;
	
	// GUI
	private JTable triggerTable;
	private JRadioButton onEnterButton;
	private JRadioButton onExitButton;
	private JRadioButton onBlock;
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	public TriggerModule() 
	{
		super("Triggers");
		
		this.nextID = 0;
		this.nextType = Trigger.ACTIVATING;
		this.triggers = new ArrayList<Trigger>(0);
		this.state = TARGETS;
		
		this.buildGUI();
		this.pack();
		this.setVisible(false);	
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public ArrayList<Trigger> getTriggers()
	{
		return triggers;
	}
	
	public void newLevel()
	{
		this.nextID = 0;
		this.nextType = Trigger.ACTIVATING;
		this.triggers = new ArrayList<Trigger>(0);
		this.state = TARGETS;
		
		updateTable();
	}
	
	public void loadedLevel(int nextID, ArrayList<Trigger> triggers)
	{
		this.nextID = nextID;
		this.nextType = Trigger.ACTIVATING;
		this.triggers = triggers;
		this.state = TARGETS;
		
		updateTable();
	}
	
	public void leftClick(int layer, Point gridLoc, boolean shiftPressed)
	{
		int newLoc[] = new int[]{layer, gridLoc.x, gridLoc.y};
		
		if(state == CREATING)
		{
			// Check if attempting to Place on existing Trigger
			for(int tIndex = 0; tIndex < triggers.size(); tIndex++)
			{
				Trigger testTrigger = triggers.get(tIndex);
				int testLoc[] = testTrigger.getLocation();
				
				if((testLoc[0] == newLoc[0]) && (testLoc[1] == newLoc[1]) && (testLoc[2] == newLoc[2]))
				{
					// Click was on current Trigger, do nothing
					setMessage("Cannot place on existing Trigger");
					return;
				}
			}
			
			// Open Spot, create new Trigger
			setMessage("Trigger Created");
			
			int whenTriggers;
			if(onEnterButton.isSelected())
			{
				whenTriggers = Trigger.ON_ENTER;
			}
			else if(onExitButton.isSelected())
			{
				whenTriggers = Trigger.ON_EXIT;
			}
			else
			{
				whenTriggers = Trigger.BLOCK_ACTIVATED;
			}
			
			
			Trigger newTrigger = new Trigger(nextID, nextType, whenTriggers, newLoc[0], newLoc[1], newLoc[2]);
			nextID++;
			triggers.add(newTrigger);
			state = TARGETS;
			updateTable();
		}
		else if(state == PLACING)
		{
			int index = triggerTable.getSelectedRow();
			
			// Check if attempting to Place on existing Trigger
			for(int tIndex = 0; tIndex < triggers.size(); tIndex++)
			{
				Trigger testTrigger = triggers.get(tIndex);
				int testLoc[] = testTrigger.getLocation();
				
				if((testLoc[0] == newLoc[0]) && (testLoc[1] == newLoc[1]) && (testLoc[2] == newLoc[2]))
				{
					// Click was on current Trigger, do nothing
					setMessage("Cannot place on existing Trigger");
					return;
				}
			}
			
			// Open Spot, update Trigger
			setMessage("Trigger Placed");
			triggers.get(index).setLocation(newLoc);
			state = TARGETS;
			updateTable();
		}
		else if(state == TARGETS)
		{
			int index = triggerTable.getSelectedRow();
			
			if(index != (-1))
			{
				if(triggers.get(index).addTarget(newLoc))
				{
					// Added new Target
					EditorMain.getInstance().getMapPanel().setTriggerTarget(true, newLoc);
					setMessage("Added Target");
				}
			}
		}
	}
	
	public void rightClick(int layer, Point gridLoc, boolean shiftPressed)
	{
		if(state == CREATING)
		{
			// Cancel Creation
			setMessage("Creation Canceled");
			state = TARGETS;
		}
		else if(state == PLACING)
		{
			// Cancel Creation
			setMessage("Placing Canceled");
			state = TARGETS;
		}
		else if(state == TARGETS)
		{
			int index = triggerTable.getSelectedRow();
			
			if(index != (-1))
			{
				int loc[] = new int[]{layer, gridLoc.x, gridLoc.y};
				
				if(triggers.get(index).removeTarget(loc))
				{
					// Removed Target
					EditorMain.getInstance().getMapPanel().setTriggerTarget(false, loc);
					setMessage("Removed Target");
				}
			}
		}		
	}
	
	public int getNextTriggerID()
	{
		return nextID;
	}
	
	// ============================================================================================
	// Protected Methods
	// ============================================================================================

	@Override
	protected void buildGUI() 
	{
		JPanel mainContent = new JPanel();
		mainContent.setLayout(new BoxLayout(mainContent, BoxLayout.Y_AXIS));
		
		mainContent.add(buildMessagePanel());
		mainContent.add(buildTablePanel());
		mainContent.add(buildButtonPanel());
		
		this.setContentPane(mainContent);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void createNewTrigger(int type)
	{
		setMessage("Place the New Trigger");
		state = CREATING;
		nextType = type;
	}
	
	private void placeTrigger()
	{
		setMessage("Place the Selected Trigger");
		state = PLACING;
	}
	
	private void deleteTrigger()
	{
		int index = triggerTable.getSelectedRow();
		
		if(index != (-1))
		{
			triggers.remove(index);
			updateTable();
		}
	}
	
	private JPanel buildTablePanel()
	{
		int tableWidth = 300;
		int tabelHeight = 250;
		
		// Set-Up Panel
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(10, 10));
		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// Create Table to display Object Data
		triggerTable = new JTable(new TriggerTableModel());
		triggerTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);	
		triggerTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
		{
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				if(!event.getValueIsAdjusting())
				{
					triggerSelected();
				}
			}
		});
		
		triggerTable.setFillsViewportHeight(true);
		
		// Create the ScollPane
		JScrollPane scroller = new JScrollPane(triggerTable);
		scroller.setPreferredSize(new Dimension(tableWidth, tabelHeight));
		
		panel.add(scroller, BorderLayout.CENTER);
		
		return panel;
	}
	
	private void updateTable()
	{
		((TriggerTableModel)triggerTable.getModel()).fireTableDataChanged();
		triggerTable.clearSelection();
		EditorMain.getInstance().getMapPanel().clearTriggerHighlights();
	}
	
	private void triggerSelected()
	{
		int index = triggerTable.getSelectedRow();

		if(index != (-1))
		{
			Trigger selectedTrigger = triggers.get(index);
			
			int targetArray[][] = selectedTrigger.getTargetArray();
			int numTargets = selectedTrigger.getTargets().size();
			
			EditorMain.getInstance().getMapPanel().clearTriggerHighlights();
			EditorMain.getInstance().getMapPanel().setActiveTrigger(selectedTrigger.getType(), selectedTrigger.getLocation());
			EditorMain.getInstance().getMapPanel().setTriggerTargets(true, targetArray, numTargets);
		}
	}

	private JPanel buildButtonPanel()
	{
		JPanel buttonPanel = new JPanel();
		
		JButton createActive = new JButton("New Activate");
		createActive.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				createNewTrigger(Trigger.ACTIVATING);
			}
		});
		
		JButton createDeac = new JButton("New Deactivate");
		createDeac.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				createNewTrigger(Trigger.DEACTIVATING);
			}
		});
		
		JButton createTogg = new JButton("New Toggle");
		createTogg.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				createNewTrigger(Trigger.TOGGLE);
			}
		});
		
		JButton placeTrigger = new JButton("Place");
		placeTrigger.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				placeTrigger();
			}
		});
			
		JButton delTrigger = new JButton("Delete");
		delTrigger.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				deleteTrigger();
			}
		});
		
		buttonPanel.add(createActive);
		buttonPanel.add(createDeac);
		buttonPanel.add(createTogg);
		buttonPanel.add(placeTrigger);
		buttonPanel.add(delTrigger);
		
		JPanel radioPanel = new JPanel();
		
		onEnterButton = new JRadioButton("On Enter", true);
		onExitButton = new JRadioButton("On Exit", false);
		onBlock = new JRadioButton("Block Activated", false);
		
		ButtonGroup radioGroup = new ButtonGroup();
		radioGroup.add(onEnterButton);
		radioGroup.add(onExitButton);
		radioGroup.add(onBlock);
		
		radioPanel.add(onEnterButton);
		radioPanel.add(onExitButton);
		radioPanel.add(onBlock);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		mainPanel.add(buttonPanel);
		mainPanel.add(radioPanel);

		return mainPanel;		
	}
	
	// ============================================================================================
	// Contained Classes
	// ============================================================================================
	
	/* ===== ObjectKitTableModel ==================================================================
	 * The AbstractTableModel is used to tell the table how to read and display its' data. This
	 * class extends the ATM and adds functionality for Triggers.
	 * ========================================================================================= */
	
	private class TriggerTableModel extends AbstractTableModel
	{
		private String[] colNames = {"ID", "Type", "Triggers", "Layer", "Grid", "# Targets"};

		public TriggerTableModel()
		{
			super();
		}
		
		@Override
		public int getColumnCount() 
		{
			return colNames.length;
		}
		
		@Override
		public String getColumnName(int col)
		{
			return colNames[col];
		}

		@Override
		public int getRowCount() 
		{
			if(triggers != null)
			{
				return triggers.size();
			}
			else
			{
				return 0;
			}
		}

		@Override
		public Object getValueAt(int row, int col) 
		{
			if(triggers != null)
			{
				if(col == 0)
				{
					return triggers.get(row).getIdNumber();
				}
				else if(col == 1)
				{
					int type = triggers.get(row).getType();
					
					if(type == Trigger.ACTIVATING)
					{
						return "Activate";
					}
					else if(type == Trigger.DEACTIVATING)
					{
						return "Deactivate";
					}
					else
					{
						return "Toggle";
					}
				}
				else if(col == 2)
				{
					int whenTriggers = triggers.get(row).getWhenTriggers();
					
					if(whenTriggers == Trigger.ON_ENTER)
					{
						return "On Enter";
					}
					else if(whenTriggers == Trigger.ON_EXIT)
					{
						return "On Exit";
					}
					else
					{
						return "Block";
					}
				}
				else if(col == 3)
				{
					return triggers.get(row).getLayer();
				}
				else if(col == 4)
				{
					int loc[] = triggers.get(row).getLocation();
					return ("(" + Integer.toString(loc[1]) + ", " + Integer.toString(loc[2]) + ")");
				}
				else if(col == 5)
				{
					return triggers.get(row).getTargets().size();
				}
				else
				{
					// Error
					return "Error 6th Col";
				}
			}
			else
			{
				return null;
			}
		}
	}

}
