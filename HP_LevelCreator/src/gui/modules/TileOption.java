package gui.modules;

// Imports
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

/**
TileOption's are swing JButtons that will represent each type of tile that can be added
to the map. It will contain the core data for the tile type is is assigned and will allow the
user, through mouse clicks, to select this type of tile for adding. 

@author 	Travis Smith
@version 	1.0 | 31 Mar 2014
@see javax.swing.JButton
*/
@SuppressWarnings("serial")
public class TileOption extends JButton
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
		
	// Size Constants
	static final public int BUTTON_WIDTH = 64;
	static final public int BUTTON_HEIGHT = 64;
		
	// Assigned Tile Data
	private char tileType;
	private String tileName;
	private int imageX;
	private int imageY;
	
	// Parent
	private final TileModule parent;
	
	// Ref to image sheet
	private final BufferedImage tileSheet;
	
		
	// ============================================================================================
	// Constructor
	// ============================================================================================
		
	protected TileOption(final TileModule parent, final char tileType, String tileName, final int imageX, final int imageY)
	{	
		super();
			
		this.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMaximumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setMinimumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
		this.setToolTipText(tileName);
		this.setFocusable(false);
		
		this.parent = parent;
		
		this.tileSheet = ImageControl.getTileSheet();
			
		this.tileType = tileType;
		this.tileName = tileName;
		this.imageX = imageX;
		this.imageY = imageY;
			
		this.addActionListener(new ActionListener() 
		{		
			public void actionPerformed(ActionEvent click)
			{
				clicked();
			}
			
		});	
	}
		
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	@Override
	public void paint(Graphics gfx)
	{
		if(tileSheet == null)
		{
			gfx.setColor(Color.BLACK);
			gfx.drawRect(this.getLocation().x, this.getLocation().y, (this.getWidth() - 1), (this.getHeight() - 1));
		}
		else
		{
			gfx.drawImage(tileSheet, 0, 0, (BUTTON_WIDTH), (BUTTON_HEIGHT),
						 (imageX * BUTTON_WIDTH), (imageY * BUTTON_HEIGHT), (((imageX + 1) * BUTTON_WIDTH)), (((imageY + 1) * BUTTON_HEIGHT)), null);
		}
	}
	
	// Getters
	public char getTileType()
	{
		return tileType;
	}
	public String getTileName()
	{
		return tileName;
	}
	public int getImageX()
	{
		return imageX;
	}
	public int getImageY()
	{
		return imageY;
	}
	
	// Setters
	public void setTileType(char tileType)
	{
		this.tileType = tileType;
	}
	public void setTileName(String tileName)
	{
		this.tileName = tileName;
	}
	public void setImageX(int imageX)
	{
		this.imageX = imageX;
	}
	public void setImageY(int imageY)
	{
		this.imageY = imageY;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private void clicked()
	{
		parent.optionClicked(this);
	}
}
