package gui.tilepops;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

import levelData.Tile;

/* ================================================================================================
 * This is the pop-up that will be used for basic tiles, or tiles that require no additional
 * set-up. This includes doors, keys, ice, etc. Basic tiles have 5 parameters.
 * 
 * parameter[0]	- 1 means it starts activated, 0 means it starts deactivated
 * parameter[1] - 1 means it blocks from NORTH, 0 means is does not block
 * parameter[2] - 1 means it blocks from EAST, 0 means is does not block
 * parameter[3] - 1 means it blocks from SOUTH, 0 means is does not block
 * parameter[4] - 1 means it blocks from WEST, 0 means is does not block
 * 
 * More complex tiles will have a specific pop-up designed for them. ALL tiles will use these
 * basic 5 parameters and special tiles will only add new ones.
 * ============================================================================================= */

@SuppressWarnings("serial")
public class BasicOptions extends JPanel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Parameter Index Constants -- Subclasses should continue this list with their parameters
	public static final int ACTIVATION = 0;
	public static final int NORTH = 1;
	public static final int EAST = 2;
	public static final int SOUTH = 3;
	public static final int WEST = 4;
	
	public static final int NUM_BASIC_PARAMS = 5;
	
	// Data
	protected int numParams;
	
	// Basic GUI
	private JCheckBox activatedBox;
	private JCheckBox northBox;
	private JCheckBox eastBox;
	private JCheckBox southBox;
	private JCheckBox westBox;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected BasicOptions() 
	{		
		super();
	
		// Basic tiles have five parameters
		this.numParams = NUM_BASIC_PARAMS;
		
		this.add(buildGUI());
	}
	
	protected BasicOptions(Tile data) 
	{		
		super();
	
		// Basic tiles have five parameters
		this.numParams = NUM_BASIC_PARAMS;
		
		this.add(buildGUI());
		
		setValues(data);
	}

	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public int getNumParams()
	{
		return this.numParams;
	}
	
	public int[] getParams() 
	{
		int params[] = new int[numParams];
		
		// Get Activation
		if(activatedBox.isSelected())
		{
			params[ACTIVATION] = 1;
		}
		else
		{
			params[ACTIVATION] = 0;
		}
		
		// Get Blocking Values
		if(northBox.isSelected())
		{
			params[NORTH] = 1;
		}
		else
		{
			params[NORTH] = 0;
		}
		
		if(eastBox.isSelected())
		{
			params[EAST] = 1;
		}
		else
		{
			params[EAST] = 0;
		}
		
		if(southBox.isSelected())
		{
			params[SOUTH] = 1;
		}
		else
		{
			params[SOUTH] = 0;
		}
		
		if(westBox.isSelected())
		{
			params[WEST] = 1;
		}
		else
		{
			params[WEST] = 0;
		}

		return params;
	}

	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildGUI() 
	{
		// Build Activated Panel
		JPanel actPanel = new JPanel();
		activatedBox = new JCheckBox("Start Activated", true);
		actPanel.add(activatedBox);
		
		// Build Blocking Panel
		JPanel blockPanel = new JPanel();
		blockPanel.setLayout(new BorderLayout());
		blockPanel.setBorder(BorderFactory.createTitledBorder("Block Entry From: "));
		
		JPanel northPanel = new JPanel();
		northBox = new JCheckBox("N", false);
		northPanel.add(northBox);
		
		eastBox = new JCheckBox("E", false);
		
		JPanel southPanel = new JPanel();
		southBox = new JCheckBox("S", false);
		southPanel.add(southBox);
		
		westBox = new JCheckBox("W", false);
		
		blockPanel.add(northPanel, BorderLayout.NORTH);
		blockPanel.add(eastBox, BorderLayout.EAST);
		blockPanel.add(southPanel, BorderLayout.SOUTH);
		blockPanel.add(westBox, BorderLayout.WEST);
		
		JPanel basicPanel = new JPanel();
		basicPanel.setLayout(new BoxLayout(basicPanel, BoxLayout.Y_AXIS));
		basicPanel.add(actPanel);
		basicPanel.add(blockPanel);
		
		return basicPanel;
	}
	
	private void setValues(Tile data)
	{
		int dataParams[] = data.getParameters();
		
		if(dataParams[0] == 1)
		{
			activatedBox.setSelected(true);
		}
		else
		{
			activatedBox.setSelected(false);
		}
		
		if(dataParams[1] == 1)
		{
			northBox.setSelected(true);
		}
		else
		{
			northBox.setSelected(false);
		}
		
		if(dataParams[2] == 1)
		{
			eastBox.setSelected(true);
		}
		else
		{
			eastBox.setSelected(false);
		}
		
		if(dataParams[3] == 1)
		{
			southBox.setSelected(true);
		}
		else
		{
			southBox.setSelected(false);
		}
		
		if(dataParams[4] == 1)
		{
			westBox.setSelected(true);
		}
		else
		{
			westBox.setSelected(false);
		}
		
	}
}
