package gui.tilepops;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import start.EditorMain;
import levelData.Tile;

/* ================================================================================================
 * This is the pop-up that will be used for Stairs tiles. The first new parameter is the animation
 * type that should be played. The second is layer these stairs move to.
 * 
 * params[5] - animation style
 * params[6] - destination layer
 * ============================================================================================= */

@SuppressWarnings("serial")
public class StairsOptions extends BasicOptions
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	private static final int NUM_STAIRS_PARAMS = 7;
	
	private static final String[] ANIM_OPS = {"Stairs Down", "Stairs Up", "Mirror"}; 
	
	// Parameter Index Constants
	public static final int ANIMATION = 5;
	public static final int DESTINATION = 6;

	// Basic GUI
	private JComboBox<String> animBox;
	private JComboBox<Integer> destBox;
	
	// ============================================================================================
	// Constructors
	// ============================================================================================
	
	protected StairsOptions() 
	{		
		super();
		
		// Stairs add two new parameters
		this.numParams = NUM_STAIRS_PARAMS;
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(buildGUI());
	}
	
	protected StairsOptions(Tile data) 
	{		
		super(data);
		
		// Stairs add two new parameters
		this.numParams = NUM_STAIRS_PARAMS;
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(buildGUI());
		
		setValues(data);
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	@Override
	public int[] getParams() 
	{
		int params[] = new int[numParams];
		
		int basicParams[] = super.getParams();
		
		// Add basic parameters to array
		for(int pIndex = 0; pIndex < BasicOptions.NUM_BASIC_PARAMS; pIndex++)
		{
			params[pIndex] = basicParams[pIndex];
		}
		
		// Add Animation and Destination Layer
		params[ANIMATION] = animBox.getSelectedIndex();
		params[DESTINATION] = (int)destBox.getSelectedItem();
		
		return params;
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private JPanel buildGUI() 
	{
		// Build Animation Panel
		JPanel animPanel = new JPanel();
		JLabel animLabel = new JLabel("Animation Style: ");
		animBox = new JComboBox<String>(ANIM_OPS);
		animPanel.add(animLabel);
		animPanel.add(animBox);
		
		
		// Build Destination Panel
		JPanel destPanel = new JPanel();
		JLabel destLabel = new JLabel("Destination: ");
		destBox = new JComboBox<Integer>(getLayerOptions());
		
		destPanel.add(destLabel);
		destPanel.add(destBox);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));	
		mainPanel.setBorder(BorderFactory.createTitledBorder("Stairs Options"));
		mainPanel.add(animPanel);
		mainPanel.add(destPanel);
		
		return mainPanel;
	}
	
	private Integer[] getLayerOptions()
	{
		int numLayers = EditorMain.getInstance().getMapPanel().getNumLayers();
		
		Integer layerArray[] = new Integer[numLayers];
		
		for(int index = 0; index < numLayers; index++)
		{
			layerArray[index] = index;
		}
		
		return layerArray;
	}
	
	private void setValues(Tile data)
	{
		int animStyle = data.getParameters()[ANIMATION];
		animBox.setSelectedIndex(animStyle);

		Integer layerId = data.getParameters()[DESTINATION];		
		destBox.setSelectedItem(layerId);
	}
}

