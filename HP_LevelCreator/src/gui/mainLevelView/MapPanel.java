package gui.mainLevelView;

// Imports

import gui.tilepops.TeleportOptions;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import start.EditorMain;
import levelData.Level;
import levelData.Tile;

/**
The MapPanel will contain the grid of tiles representing the level. Users will be able to modify
the tiles by right or left clicking and the behavior of the click will be dictated by the 
current "mode" that the EditorMain is in.
<p>

@author 	Travis Smith
@version 	1.0 | 19 Mar 2014
*/
@SuppressWarnings("serial")
public class MapPanel extends JInternalFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================

	// GUI Members
	private static final Dimension MAX_SIZE = new Dimension(600, 600);
	private JTabbedPane layerTabs;
	private LayerPanel layerPanels[];
	
	// Level Data
	private int numLayers;
	private int levelDimension;
	private int sectionDimension;
	private String levelName;
	private int rotationType;
	private int numTeleporters;
	private ArrayList<Point> teleporters;	// X = id, Y = destination
	
	private boolean hasExit;
	private int exitLoc[];
	private boolean hasStart;
	private int startLoc[];
	
	// Module Data
	private Point firstCorner;
	private Point secondCorner;
	private Point topLeft;
	private Point botRight;

	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public MapPanel()
	{
		super("Map", false, false); 
		this.setLocation(400, 0);
		
		// Level Data
		numLayers = 1;
		levelDimension = 2;
		sectionDimension = 3;
		levelName = "DefaultName";
		rotationType = Level.WHEEL;
		numTeleporters = 0;	
		teleporters = new ArrayList<Point>(0);
		
		// Start/Exit Data
		hasExit = false;
		exitLoc = new int[]{0, 0, 0};
		hasStart = false;
		startLoc = new int[]{0, 0, 0};
		
		buildGUI();
		
		// Module Data
		resetPathCorners();
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================

	public void newLevel()
	{
		// Display Popup
		LvlProperties popup = new LvlProperties();
		
		// Create Pop-up and get user input
		int optionChoice = JOptionPane.showConfirmDialog(null, popup, "New Level", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		
		if(optionChoice == JOptionPane.OK_OPTION)
		{	
			Level newLevel = popup.buildLevel();
			numLayers = newLevel.getNumLayers();
			levelDimension = newLevel.getLevelDimension();
			sectionDimension = newLevel.getSectionDimension();
			levelName = newLevel.getLevelName();
			rotationType = newLevel.getRotationType();
			numTeleporters = 0;
			teleporters = new ArrayList<Point>(0);
			hasExit = false;
			hasStart = false;
			
			buildGUI();
			
			EditorMain.getInstance().getTriggerModule().newLevel();
			EditorMain.getInstance().setMode(EditorMain.PATHING);
		}	
	}
	
	public void editLevel()
	{
		// Display Popup
		LvlProperties popup = new LvlProperties(getLevelData());
		
		// Create Pop-up and get user input
		int optionChoice = JOptionPane.showConfirmDialog(null, popup, "Edit Level", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
		
		if(optionChoice == JOptionPane.OK_OPTION)
		{	
			Level newData = popup.buildLevel();
			levelName = newData.getLevelName();
			rotationType = newData.getRotationType();
		}		
	}
	
	public void levelLoaded(Level levelData, Tile tileData[][][])
	{
		numLayers = levelData.getNumLayers();
		levelDimension = levelData.getLevelDimension();
		sectionDimension = levelData.getSectionDimension();
		levelName = levelData.getLevelName();
		rotationType = levelData.getRotationType();
		numTeleporters = levelData.getNumTeleporters();
		teleporters = new ArrayList<Point>(numTeleporters);
		hasExit = false;
		hasStart = false;
		
		// Find Teleporters
		int gridDimension = (levelDimension * sectionDimension);
		
		for(int lIndex = 0; lIndex < numLayers; lIndex++)
		{
			for(int rIndex = 0; rIndex < gridDimension; rIndex++)
			{
				for(int cIndex = 0; cIndex < gridDimension; cIndex++)
				{
					Tile currentTile = tileData[lIndex][cIndex][rIndex];

					if(currentTile.getType() == Tile.TELE_TILE)
					{
						int id = currentTile.getParameters()[TeleportOptions.NAME];
						int dest = currentTile.getParameters()[TeleportOptions.DESTINATION];
						
						Point newTele = new Point(id, dest);		
						teleporters.add(newTele);
					}
				}	
			}		
		}
		
		buildGUI(tileData);

	}
	
	public void setActiveTrigger(int type, int location[])
	{
		clearTriggerHighlights();
		layerPanels[location[0]].getButtonGrid()[location[1]][location[2]].setTrigger(true, type);
	}
	
	public void setTriggerTargets(boolean state, int targets[][], int numTargets)
	{
		for(int index = 0; index < numTargets; index++)
		{
			int currentTarget[] = targets[index];
			
			layerPanels[currentTarget[0]].getButtonGrid()[currentTarget[1]][currentTarget[2]].setTriggerTarget(state);
		}
	}
	
	public void setTriggerTarget(boolean state, int loc[])
	{
		layerPanels[loc[0]].getButtonGrid()[loc[1]][loc[2]].setTriggerTarget(state);
	}
	
	public void clearTriggerHighlights()
	{
		for(int index = 0; index < numLayers; index++)
		{
			layerPanels[index].clearHighlights();
		}
	}

	public Level getLevelData()
	{
		int nextTrigID = EditorMain.getInstance().getTriggerModule().getNextTriggerID();
		Level newData = new Level(levelName, numLayers, levelDimension, sectionDimension, rotationType, numTeleporters, nextTrigID);
		
		return newData;
	}
	
	public Tile[][][] getAllTileData()
	{
		int tileGridDim = (levelDimension * sectionDimension);
		
		Tile data[][][] = new Tile[numLayers][tileGridDim][tileGridDim];	
		
		for(int lIndex = 0; lIndex < numLayers; lIndex++)
		{
			data[lIndex] = layerPanels[lIndex].getTileData();
		}
		
		return data;
	}
	
	public int getNextTeleportID()
	{
		return numTeleporters;
	}
	
	public int[] getTeleporterIds() 
	{
		int ids[] = new int[teleporters.size()];
		
		for(int index = 0; index < teleporters.size(); index++)
		{
			ids[index] = teleporters.get(index).x;
		}
		
		return ids;
	}
	
	public int getTeleporterListSize()
	{
		return teleporters.size();
	}
	
	public int getNumLayers() 
	{
		return numLayers;
	}

	// ============================================================================================
	// Protected Methods
	// ============================================================================================
	
	protected void setPathCorner(Point gridCoord, boolean leftClick) 
	{
		if(firstCorner == null)
		{
			firstCorner = gridCoord;
		}
		else
		{
			secondCorner = gridCoord;
			setAreaCorners();
			
			MapButton grid[][] = layerPanels[layerTabs.getSelectedIndex()].getButtonGrid();
			Tile newData;
			
			for(int rIndex = topLeft.y; rIndex <= botRight.y; rIndex ++)
			{
				for(int cIndex = topLeft.x; cIndex <= botRight.x; cIndex++)
				{
					if(leftClick)
					{
						newData = Tile.buildBasicTile(Tile.FLOOR_TILE);
					}
					else
					{
						newData = Tile.buildBasicTile(Tile.WALL_TILE);
					}
					
					grid[cIndex][rIndex].setTileData(newData);
				}
			}
			
			resetPathCorners();
		}
		
	}
	
	protected void resetPathCorners() 
	{
		firstCorner = null;
		secondCorner = null;
		topLeft = null;
		botRight = null;
	}
	
	protected void placeStart(int layer, Point loc) 
	{
		if(hasStart)
		{
			
			Tile data = Tile.buildBasicTile(Tile.WALL_TILE);
			layerPanels[startLoc[0]].getButtonGrid()[startLoc[1]][startLoc[2]].setTileData(data);
			hasStart = false;
		}
		
		hasStart = true;
		startLoc[0] = layer;
		startLoc[1] = loc.x;
		startLoc[2] = loc.y;
	}
	
	protected void resetStart() 
	{
		hasStart = false;
	}
	
	protected void placeExit(int layer, Point loc) 
	{
		if(hasExit)
		{
			Tile data = Tile.buildBasicTile(Tile.WALL_TILE);
			layerPanels[exitLoc[0]].getButtonGrid()[exitLoc[1]][exitLoc[2]].setTileData(data);
			hasExit = false;
		}
		
		hasExit = true;
		exitLoc[0] = layer;
		exitLoc[1] = loc.x;
		exitLoc[2] = loc.y;
	}
	
	protected void resetExit() 
	{
		hasExit = false;
	}
	
	protected void addTeleporter(int dest)
	{
		Point newTele = new Point(numTeleporters, dest);
		teleporters.add(newTele);
		numTeleporters++;
	}
	
	protected void removeTeleporter(int id)
	{
		for(int lIndex = 0; lIndex < teleporters.size(); lIndex++)
		{
			if(teleporters.get(lIndex).y == id)
			{
				teleporters.get(lIndex).y = (-1);
			}
			
			if(teleporters.get(lIndex).x == id)
			{
				teleporters.remove(lIndex);
			}
		}
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================

	private void buildGUI()
	{
		this.setVisible(false);
	
		// Set-Up Main Content Pane
		layerTabs = new JTabbedPane();
		layerTabs.setFocusable(false);
		
		layerPanels = new LayerPanel[numLayers];
		
		for(int index = 0; index < numLayers; index++)
		{
			layerPanels[index] = new LayerPanel(index, levelDimension, sectionDimension);
			layerTabs.addTab(("Layer: " + index), layerPanels[index]);
		}
		
		layerTabs.setSelectedIndex(0);
		
		JScrollPane mainScroll = new JScrollPane(layerTabs);
		
		if(layerTabs.getPreferredSize().getWidth() > MAX_SIZE.getWidth())
		{
			this.setPreferredSize(MAX_SIZE);
		}
		else 
		{
			mainScroll.setPreferredSize(layerTabs.getPreferredSize());
			this.setPreferredSize(null);
		}
		
		this.setContentPane(mainScroll);
		this.pack();
		this.setVisible(true);
	}
	
	private void buildGUI(Tile data[][][])
	{
		this.setVisible(false);
	
		// Set-Up Main Content Pane
		layerTabs = new JTabbedPane();
		layerTabs.setFocusable(false);
		
		layerPanels = new LayerPanel[numLayers];
		
		for(int index = 0; index < numLayers; index++)
		{
			layerPanels[index] = new LayerPanel(index, levelDimension, sectionDimension, data[index]);
			layerTabs.addTab(("Layer: " + index), layerPanels[index]);
		}
		
		layerTabs.setSelectedIndex(0);
		
		JScrollPane mainScroll = new JScrollPane(layerTabs);
		
		if(layerTabs.getPreferredSize().getWidth() > MAX_SIZE.getWidth())
		{
			this.setPreferredSize(MAX_SIZE);
		}
		else 
		{
			mainScroll.setPreferredSize(layerTabs.getPreferredSize());
			this.setPreferredSize(null);
		}

		this.setContentPane(mainScroll);
		this.pack();
		this.setVisible(true);
	}

	private void setAreaCorners()
	{
		// Compare passed corners; set the top left and bottom right corners for easy array traversal
		int topLeftX;
		int topLeftY;
		int botRightX;
		int botRightY;
		
		if(firstCorner.x <= secondCorner.x)
		{
			topLeftX = firstCorner.x;
			botRightX = secondCorner.x;
		}
		else
		{
			topLeftX = secondCorner.x;
			botRightX = firstCorner.x;
		}
		
		if(firstCorner.y <= secondCorner.y)
		{
			topLeftY = firstCorner.y;
			botRightY = secondCorner.y;
		}
		else
		{
			topLeftY = secondCorner.y;
			botRightY = firstCorner.y;
		}
		
		topLeft = new Point(topLeftX, topLeftY);
		botRight = new Point(botRightX, botRightY);	
	}
}
