package gui.mainLevelView;

//Imports
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import levelData.Level;

/**
The LvlProperties class will be a pop-up window that allows the user to edit the parameters of the
current level. It will extend a JPanel from the swing library and hold GUI elements to set the 
dimensions of each section of the level as well as how many sections per level there will be.
<p>
As new features are added to the levels, the options to set and edit them will be added to this
window. 

@author 	Travis Smith
@version 	1.0 | 24 Mar 2014
@see javax.swing.JPanel
*/
@SuppressWarnings("serial")
public class LvlProperties extends JPanel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// Static Final Selection Options
	public static final String ROTATION_OPS[] = {"Wheel", "Group", "Single"};
	
	public static final Integer LAYER_OPS[] = {1, 2, 3};
	public static final String SECTION_OPS[] = {"3x3", "4x4", "5x5", "6x6"};
	public static final String LEVEL_OPS[] = {"2x2", "3x3"};
	
	// GUI Components
	private JComboBox<String> rotationTypeBox;
	private JComboBox<Integer> layersBox;
	private JComboBox<String> sectionSizeBox;
	private JComboBox<String> levelSizeBox;
	private JTextField nameField;
	
	
	public String getLevelName()
	{
		return nameField.getText();
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	/**
	 * Class Default Constructor. The default constructor will build the GUI for the panel without
	 * setting any values, instead the default settings for levels will be used. This constructor
	 * should be called when creating a new level. 
	 */
	public LvlProperties()
	{
		super();
		
		buildWindowGUI();
	}
	
	/**
	 * Class Constructor. This constructor will take an already existing Level and use it to set the
	 * initial values of the GUI elements. This constructor should be used when editing an existing
	 * level.
	 * 
	 * @param currentLevel - the instance of Level being edited. 
	 */
	public LvlProperties(Level currentLevel)
	{
		super();
		
		buildWindowGUI();
		setValues(currentLevel);
		
		layersBox.setEnabled(false);
		levelSizeBox.setEnabled(false);
		sectionSizeBox.setEnabled(false);
	}

	public Level buildLevel()
	{
		String name = nameField.getText();
		
		int layers = (int)layersBox.getSelectedItem();
		
		int lvlDim = getLevelDimension();
		
		int sectionDim = getSectionDimension();
		
		int rType = (int)rotationTypeBox.getSelectedIndex();

		Level newLevel = new Level(name, layers, lvlDim, sectionDim, rType, 0, 0);
		
		return newLevel;
	}

	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	private int getSectionDimension()
	{
		int index = sectionSizeBox.getSelectedIndex();
		
		if(index == 0)
		{
			return 3;
		}
		else if(index == 1)
		{
			return 4;
		}
		else if(index == 2)
		{
			return 5;
		}
		else if(index == 3)
		{
			return 6;
		}
		else
		{
			// Return 3 as Default
			return 3;
		}
	}
	
	private int getSectionBoxIndex(int dimension)
	{
		if(dimension == 3)
		{
			return 0;
		}
		else if(dimension == 4)
		{
			return 1;
		}
		else if(dimension == 5)
		{
			return 2;
		}
		else if(dimension == 6)
		{
			return 3;
		}
		else
		{
			// Return 0 as Default
			return 0;
		}	
	}
	
	private int getLevelDimension()
	{
		int index = levelSizeBox.getSelectedIndex();
		
		if(index == 0)
		{
			return 2;
		}
		else if(index == 1)
		{
			return 3;
		}
		else
		{
			// Return 2 as Default
			return 2;
		}
	}
	
	private int getLevelBoxIndex(int dimension)
	{
		if(dimension == 2)
		{
			return 0;
		}
		else if(dimension == 3)
		{
			return 1;
		}
		else
		{
			// Return 0 as Default
			return 0;
		}
	}
	
	private void setValues(Level data)
	{
		String name = data.getLevelName();
		int layers = data.getNumLayers();
		int lvlDim = data.getLevelDimension();
		int secDim = data.getSectionDimension();
		int rType = data.getRotationType();
		
		nameField.setText(name);
		layersBox.setSelectedItem((Integer)layers);
		sectionSizeBox.setSelectedIndex(getSectionBoxIndex(secDim));
		levelSizeBox.setSelectedIndex(getLevelBoxIndex(lvlDim));
		rotationTypeBox.setSelectedIndex(rType);
	}
	
	/**
	 * Will construct all the GUI components that will be added to the pop-up JPanel. This method wll
	 * be responsible for the layout and sizes of all GUI components. 
	 * <p>
	 * This method will not set any values on the GUI elements and they will keep their default values
	 * until changed in another method.
	 */
	private void buildWindowGUI()
	{		
 		// Initialize Option Boxes
 		rotationTypeBox = new JComboBox<String>(ROTATION_OPS);
 		rotationTypeBox.setAlignmentX(Component.LEFT_ALIGNMENT);
 		((JLabel)rotationTypeBox.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
 		
 		layersBox = new JComboBox<Integer>(LAYER_OPS);
 		layersBox.setAlignmentX(Component.LEFT_ALIGNMENT);
 		((JLabel)layersBox.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
 		
 		sectionSizeBox = new JComboBox<String>(SECTION_OPS);
 		sectionSizeBox.setAlignmentX(Component.LEFT_ALIGNMENT);
 		((JLabel)sectionSizeBox.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
 		
 		levelSizeBox = new JComboBox<String>(LEVEL_OPS);
 		levelSizeBox.setAlignmentX(Component.LEFT_ALIGNMENT);
 		((JLabel)levelSizeBox.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
 		
 		// Build Size Panel
 		JLabel layerLabel = new JLabel("Number of Layers: ");
 		layerLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
 		JLabel sectionSizeLabel = new JLabel("Section Size:");
 		sectionSizeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
 		JLabel levelSizeLabel = new JLabel("Number of Sections:");
 		levelSizeLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
 		
 		JPanel sizePanel = new JPanel();
 		sizePanel.setLayout(new BoxLayout(sizePanel, BoxLayout.Y_AXIS));
 		sizePanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Size Options")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
 		sizePanel.add(layerLabel);
 		sizePanel.add(layersBox);
 		sizePanel.add(sectionSizeLabel);
 		sizePanel.add(sectionSizeBox);
 		sizePanel.add(levelSizeLabel);
 		sizePanel.add(levelSizeBox);
 		
 		// Build Level Specific Options Panel
 		JPanel optionsPanel = new JPanel();
 		optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
 		optionsPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Level Options")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
 		optionsPanel.add(new JLabel("Rotation Style:"));
 		optionsPanel.add(rotationTypeBox);
 		optionsPanel.add(Box.createVerticalGlue());
 		
 		// Build Level Data Panel
 		nameField = new JTextField("New Level");
 		nameField.setAlignmentX(Component.LEFT_ALIGNMENT);
 		nameField.setMargin(new Insets(0, 3, 0, 3));
 		JLabel nameLabel = new JLabel("Name:");
 		nameLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
 		
 		JPanel dataPanel = new JPanel();
 		dataPanel.setLayout(new BoxLayout(dataPanel, BoxLayout.Y_AXIS));
 		dataPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Level Data")
				, BorderFactory.createEmptyBorder(3, 3, 3, 3)));
 		dataPanel.add(nameLabel);
 		dataPanel.add(nameField);
 		
 		// Set-Up main panel and add sub-panels
 		this.setLayout(new BorderLayout());
 		
 		JPanel corePanel = new JPanel();
 		corePanel.setLayout(new BoxLayout(corePanel, BoxLayout.X_AXIS));
 		corePanel.add(sizePanel);
 		corePanel.add(optionsPanel);
 		
 		this.add(dataPanel, BorderLayout.NORTH);
 		this.add(corePanel, BorderLayout.CENTER);	
	}
}
