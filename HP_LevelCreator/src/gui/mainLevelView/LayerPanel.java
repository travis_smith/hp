package gui.mainLevelView;

import java.awt.GridLayout;
import java.awt.Point;

import javax.swing.JPanel;

import levelData.Tile;

@SuppressWarnings("serial")
public class LayerPanel extends JPanel
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	private int layerIndex;
	private int gridDimension;
	private MapButton buttonGrid[][];
	
	// ============================================================================================
	// Constructor
	// ============================================================================================
	
	public LayerPanel(int layerIndex, int levelDimension, int sectionDimension)
	{
		super();
		
		this.layerIndex = layerIndex;
		this.gridDimension = (levelDimension * sectionDimension);
		
		// Setup Main Panel
		this.setLayout(new GridLayout(levelDimension, levelDimension, MapButton.BUTTON_HEIGHT, MapButton.BUTTON_WIDTH));
		
		// Setup Button Grid
		this.buttonGrid = new MapButton[gridDimension][gridDimension];
		for(int rIndex = 0; rIndex < gridDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < gridDimension; cIndex++)
			{
				Point gridLoc = new Point(cIndex, rIndex);
				this.buttonGrid[cIndex][rIndex] = new MapButton(layerIndex, gridLoc);
			}	
		}	
		
		// Build Section Panels
		for(int secRIndex = 0; secRIndex < levelDimension; secRIndex++)
		{
			for(int secCIndex = 0; secCIndex < levelDimension; secCIndex++)
			{
				JPanel section = new JPanel();
				section.setLayout(new GridLayout(sectionDimension, sectionDimension, 0, 0));
				
				for(int tileRIndex = 0; tileRIndex < sectionDimension; tileRIndex++)
				{
					for(int tileCIndex = 0; tileCIndex < sectionDimension; tileCIndex++)
					{
						int buttonCol = (sectionDimension * secCIndex) + tileCIndex;
						int buttonRow = (sectionDimension * secRIndex) + tileRIndex;
						
						section.add(buttonGrid[buttonCol][buttonRow]);
					}
				}
				
				this.add(section);
			}
		}
	}
	
	public LayerPanel(int layerIndex, int levelDimension, int sectionDimension, Tile[][] loadedData)
	{
		super();
		
		this.layerIndex = layerIndex;
		this.gridDimension = (levelDimension * sectionDimension);
		
		// Setup Main Panel
		this.setLayout(new GridLayout(levelDimension, levelDimension, MapButton.BUTTON_HEIGHT, MapButton.BUTTON_WIDTH));
		
		// Setup Button Grid
		this.buttonGrid = new MapButton[gridDimension][gridDimension];
		for(int rIndex = 0; rIndex < gridDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < gridDimension; cIndex++)
			{
				Point gridLoc = new Point(cIndex, rIndex);
				this.buttonGrid[cIndex][rIndex] = new MapButton(layerIndex, gridLoc, loadedData[cIndex][rIndex]);
			}	
		}	
		
		// Build Section Panels
		for(int secRIndex = 0; secRIndex < levelDimension; secRIndex++)
		{
			for(int secCIndex = 0; secCIndex < levelDimension; secCIndex++)
			{
				JPanel section = new JPanel();
				section.setLayout(new GridLayout(sectionDimension, sectionDimension, 0, 0));
				
				for(int tileRIndex = 0; tileRIndex < sectionDimension; tileRIndex++)
				{
					for(int tileCIndex = 0; tileCIndex < sectionDimension; tileCIndex++)
					{
						int buttonCol = (sectionDimension * secCIndex) + tileCIndex;
						int buttonRow = (sectionDimension * secRIndex) + tileRIndex;
						
						section.add(buttonGrid[buttonCol][buttonRow]);
					}
				}
				
				this.add(section);
			}
		}		
	}
	
	protected void clearHighlights()
	{
		for(int rIndex = 0; rIndex < gridDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < gridDimension; cIndex++)
			{
				buttonGrid[cIndex][rIndex].setTrigger(false, (-1));
				buttonGrid[cIndex][rIndex].setTriggerTarget(false);
			}
		}
	}
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	public Tile[][] getTileData()
	{
		Tile tileData[][] = new Tile[gridDimension][gridDimension];
		
		for(int rIndex = 0; rIndex < gridDimension; rIndex++)
		{
			for(int cIndex = 0; cIndex < gridDimension; cIndex++)
			{
				tileData[cIndex][rIndex] = buttonGrid[cIndex][rIndex].getTileData();
			}
		}
		
		return tileData;
	}
	
	public MapButton[][] getButtonGrid()
	{
		return buttonGrid;
	}
	
	public int getLayerIndex()
	{
		return layerIndex;
	}
}
