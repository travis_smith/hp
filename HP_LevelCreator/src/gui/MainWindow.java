package gui;

// Imports
import gui.modules.EditorModule;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import start.EditorMain;

/**
The MainWindow will extend a JFrame and act as the container for the rest of the applications 
GUI elements. Upon creation, the MainWindow will initialize all panels. 

@author 	Travis Smith
@version 	1.0 | 18 April 2014
@see 		javax.swing.JFrame
*/

@SuppressWarnings("serial")
public class MainWindow extends JFrame
{
	// ============================================================================================
	// Data Members
	// ============================================================================================
	
	// GUI
	private JDesktopPane mainContent;
	
	// ============================================================================================
	// Public Methods
	// ============================================================================================
	
	/**
	 * Class constructor. Will initialize all GUI members and prepare the MainWindow for user 
	 * interaction.
	 */
	public MainWindow()
	{
		// Initialize Frame
		super();
		this.setTitle("Hocus Pocus Level Creator");
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent event)
			{
				EditorMain.getInstance().quitClicked();		
			}
			
		});

		BuildMenuBar();
		
		// Initialize Main Content
		mainContent = new JDesktopPane();
		mainContent.setLayout(null);
		mainContent.setPreferredSize(new Dimension(1000, 800));
		mainContent.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
		
		// Display
		this.setContentPane(mainContent);
		this.pack();
		this.setVisible(true);
	}
	
	// Module Adding
	public void addModule(EditorModule module)
	{
		mainContent.add(module);
	}
	
	// ============================================================================================
	// Private Methods
	// ============================================================================================
	
	/**
	 * Will set-up and add the main menu bar to the MainWindow. The menu will contain the options
	 * to load, edit, and save levels.
	 */
	private void BuildMenuBar()
	{
		// Create Menu Option Items
		JMenuItem newLevel = new JMenuItem("New");
		newLevel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().newLevelClicked();
			}
		});
		
		JMenuItem loadLevel = new JMenuItem("Load");
		loadLevel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().loadLevelClicked();
			}
		});
		
		JMenuItem saveLevel = new JMenuItem("Save");
		saveLevel.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().saveLevelClicked();
			}
		});
		
		JMenuItem saveLevelAs = new JMenuItem("SaveAs...");
		saveLevelAs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().saveLevelAsClicked();
			}
		});
		
		JMenuItem quitButton = new JMenuItem("Quit");
		quitButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().quitClicked();
			}
		});
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(newLevel);
		fileMenu.add(loadLevel);
		fileMenu.add(saveLevel);
		fileMenu.add(saveLevelAs);
		fileMenu.addSeparator();
		fileMenu.add(quitButton);
		
		JMenuItem basicButton = new JMenuItem("Basic Layout");
		basicButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().setMode(EditorMain.PATHING);
			}
		});
		
		JMenuItem tilesButton = new JMenuItem("Special Tiles");
		tilesButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().setMode(EditorMain.TILES);
			}
		});
		
		JMenuItem triggerButton = new JMenuItem("Triggers");
		triggerButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().setMode(EditorMain.TRIGGERS);
			}
		});
		
		JMenuItem enemiesButton = new JMenuItem("Enemies");
		enemiesButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().setMode(EditorMain.ENEMIES);
			}
		});
		
		JMenu modeMenu = new JMenu("Edit Modes");
		modeMenu.add(basicButton);
		modeMenu.add(tilesButton);
		modeMenu.add(triggerButton);
		modeMenu.add(enemiesButton);
		
		JMenuItem propertiesButton = new JMenuItem("Properties");
		propertiesButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().propertiesClicked();
			}
		});
		
		JMenuItem testButton = new JMenuItem("Test Level");
		testButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent action)
			{
				EditorMain.getInstance().testLevelClicked();
			}
		});
		
		JMenu levelMenu = new JMenu("Level");
		levelMenu.add(propertiesButton);
		levelMenu.add(testButton);

		JMenuBar menu = new JMenuBar();
		menu.add(fileMenu);	
		menu.add(modeMenu);
		menu.add(levelMenu);
	
		this.setJMenuBar(menu);
	}	
}
	

