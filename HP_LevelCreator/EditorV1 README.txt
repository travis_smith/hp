

New this version:

Added "Basic Layout" and "Special Tiles" modes. These modes are used to quickly build simple levels in the editor. To change mode click the EditorModes menu on the frames toolbar and select the one you want. Currently the other two modes are disabled. 

2 New Tiles have been added: Teleporters and Stairs

The layers have been added to the levels

All tiles now have 5 basic parameters, special tiles will add on to this array.
The five basic parameters are startActivated, and the booleans controlling entering the tile from the four directions.

Notes for Designers:

Your old XML's won't load with this new version. You should save them into a different folder and once I finish teh updater you can update them. If they are simple enough, you can just rebuild them and save them in the new version.

Hovering your mouse over teleporter tiles will tell you their ID number. Teleporters can currently be assigned themselves as a destination, Handerson will need to handle this error in the game engine. Try not to do that.
Teleporters can be assigned to teleports on different levels. 

Stairs can be assigned an animation style and the layer destination. These are independent. If you assign stairs down, and tell it go up. It will display walking down then move the player up. I doubt the player will notice.
The bottom layer is considered layer 0 and they build up. Stairs currently don't ask for a col or row, instead they will move to the same col and row on the specified layer, IE straight up or down.

This editor will not save you from yourself. It's very easy to write over an existing tile by accident, it will not ask you "are you sure?" Just click carefully for now.  

Resizing was not implemented due to the time needed to properly test and debug. Ill implement this when the editor is "done" and im simply polishing. Make sure you start at the right size for now.

SaveAs doesn't do anything. If you want to save as a different name, change the name with the preferences pop-up

In special tile mode, basic tiles will no issue a pop-up to customize basic tiles, right click them. Complex tiles will issue the popup when you try to place, and can be edited again with a right click. 

Notes for Handerson:

Im going to put a new package v1FilIeO in your project with the new xml stuff, i'll use different names so it doesn't cause issues. Biggest difference is that I removed sections since they mean nothing to the editor, and replaced them with layers. You will have to redo your constructors with the new LevelKits.

I use lower case for simple tiles and upper case for Complex tiles. You will need to check for upper/lower from now on when loading.